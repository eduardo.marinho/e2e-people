const axios = require('axios');
var config = require('../config.json');
const path = require('path');
const https = require('https');

exports.GerarJsonRetorno = GerarJsonRetorno;
exports.SalvarCampoNaoParametrizado = SalvarCampoNaoParametrizado;
exports.SalvarDossie = SalvarDossie;
exports.ObterDossie = ObterDossie;
exports.RegistrarLog = RegistrarLog;
exports.SalvarNuDossie = SalvarNuDossie;
exports.SalvarNuControleCadastramento = SalvarNuControleCadastramento;
//exports.KafkaProducer = KafkaProducer;
exports.DocumentoDownload = DocumentoDownload;
//exports.SalvarRetorno = SalvarRetorno;
exports.SalvarPrint = SalvarPrint;

var apiE2ePersistencia;

switch (config.ambiente){
    case "dev": 
        apiE2ePersistencia = config.apiE2ePersistencia.dev;
        break;
    case "hom":
        apiE2ePersistencia = config.apiE2ePersistencia.hom;
        break;
    case "prod":
        apiE2ePersistencia = config.apiE2ePersistencia.prod;
        break;
}


async function GerarJsonRetorno(idDossie){
    let urlApi = `${apiE2ePersistencia.url}/api/cci/gerarJsonRetorno`;
    let ret;
    let jsonParam = {
        "chave-autenticacao": apiE2ePersistencia.chave,
        "idDossie": idDossie
    }    
    
    //At request level
    const agent = new https.Agent({
            rejectUnauthorized: false
    });

    await axios.post(urlApi, jsonParam, { httpsAgent: agent}).then(res => {
        ret = res;
    }).catch(error => {
        console.error(error)
        ret = error;
    });

    return ret; 
}

async function SalvarCampoNaoParametrizado(campo, valor, idDossie){
    let urlApi = `${apiE2ePersistencia.url}/api/cci/salvarCampoNaoParametrizado`;
    let ret;
    let jsonParam = {
        "chave-autenticacao": apiE2ePersistencia.chave,
        "campo": "Campo não parametrizado - " + campo,
        "valor": valor,
        "idDossie": idDossie
    }     
    
    //At request level
    const agent = new https.Agent({
            rejectUnauthorized: false
    });

    await axios.post(urlApi, jsonParam, { httpsAgent: agent}).then(res => {
        ret = res;
    }).catch(error => {
        console.error(error)
        ret = error;
    });

    return ret; 
}

async function SalvarDossie(jsonMessage){
    let urlApi = `${apiE2ePersistencia.url}/api/cci/salvarDossie`;
    let ret;
    let jsonParam = {
        "chave-autenticacao": apiE2ePersistencia.chave,
        "jsonMessage": jsonMessage
    }    
    
    //At request level
    const agent = new https.Agent({
            rejectUnauthorized: false
    });
    
    await axios.post(urlApi, jsonParam, { httpsAgent: agent}).then(res => {
        ret = res;
    }).catch(error => {
        console.error(error)
        ret = error;
    });

    return ret; 
}

async function ObterDossie(hostname, instancia){
    
    let urlApi = `${apiE2ePersistencia.url}/api/cci/obterDossie`;
    let ret;
    let jsonParam = {
        "chave-autenticacao": apiE2ePersistencia.chave,
        "hostname": hostname,
        "instancia": instancia
    }

    //At request level
    const agent = new https.Agent({
            rejectUnauthorized: false            
    });    
    
    await axios.post(urlApi, jsonParam, { httpsAgent: agent }).then(res => {
        ret = res;
    }).catch(error => {
        console.error(error)
        ret = error;
    });

    return ret; 
}

async function RegistrarLog(hostname, idDossie, inicio, fim, status, sucesso, mensagem){
    
    let urlApi = `${apiE2ePersistencia.url}/api/cci/registrarLog`;
    let ret;
    let jsonParam = {
        "chave-autenticacao": apiE2ePersistencia.chave,
        "hostname": hostname,
        "idDossie": idDossie,
        "inicio": inicio,
        "fim": fim,
        "status": status,
        "sucesso": sucesso,
        "mensagem": mensagem
    }    

    //At request level
    const agent = new https.Agent({
            rejectUnauthorized: false
    });    
    
    await axios.post(urlApi, jsonParam, { httpsAgent: agent}).then(res => {
        ret = res;
    }).catch(error => {
        console.error(error)
        ret = error;
    });

    return ret; 
}

async function SalvarNuDossie(idDossie, nuDossie){
    let urlApi = `${apiE2ePersistencia.url}/api/cci/salvarNuDossie`;
    let ret;
    let jsonParam = {
        "chave-autenticacao": apiE2ePersistencia.chave,
        "idDossie": idDossie,
        "nuDossie": nuDossie
    } 
    
    //At request level
    const agent = new https.Agent({
            rejectUnauthorized: false
    });

    await axios.post(urlApi, jsonParam, { httpsAgent: agent}).then(res => {        
        ret = res;
    }).catch(error => {
        console.error(error)
        ret = error;
    });

    return ret; 
}

async function SalvarNuControleCadastramento(idDossie, nuControleCadastramento){
    let urlApi = `${apiE2ePersistencia.url}/api/cci/salvarNuControleCadastramento`;
    let ret;
    let jsonParam = {
        "chave-autenticacao": apiE2ePersistencia.chave,
        "idDossie": idDossie,
        "nuControleCadastramento": nuControleCadastramento
    } 
    
    //At request level
    const agent = new https.Agent({
            rejectUnauthorized: false
    });

    await axios.post(urlApi, jsonParam, { httpsAgent: agent}).then(res => {
        ret = res;
    }).catch(error => {
        console.error(error)
        ret = error;
    });

    return ret; 
}

// async function KafkaProducer(json, key){
    
//     let urlApi = `${apiE2ePersistencia.url}/api/kafka/producer`;
//     let ret;
//     let jsonParam = {
//         "chave-autenticacao": apiE2ePersistencia.chave,
//         "json": json,
//         "key": key
//     }    

//     //At request level
//     const agent = new https.Agent({
//             rejectUnauthorized: false
//     });    
    
//     await axios.post(urlApi, jsonParam, { httpsAgent: agent}).then(res => {
//         ret = res;
//     }).catch(error => {
//         console.error(error)
//         ret = error;
//     });

//     return ret; 
// }

async function DocumentoDownload(fileUrl){
    let urlApi = `${apiE2ePersistencia.url}/api/documento/download`;
    let ret;
    let jsonParam = {
        "chave-autenticacao": apiE2ePersistencia.chave,
        "fileUrl": fileUrl        
    } 
    
    //At request level
    const agent = new https.Agent({
            rejectUnauthorized: false
    });

    await axios.post(urlApi, jsonParam, { httpsAgent: agent}).then(res => {
        ret = res;
    }).catch(error => {
        console.error(error)
        ret = error;
    });

    return ret; 
}

// async function SalvarRetorno(idDossie, dsJsonRetorno){
//     let urlApi = `${apiE2ePersistencia.url}/api/cci/salvarRetorno`;
//     let ret;
//     let jsonParam = {
//         "chave-autenticacao": apiE2ePersistencia.chave,
//         "idDossie": idDossie,
//         "dsJsonRetorno": dsJsonRetorno
//     } 
    
//     //At request level
//     const agent = new https.Agent({
//             rejectUnauthorized: false
//     });

//     await axios.post(urlApi, jsonParam, { httpsAgent: agent}).then(res => {        
//         ret = res;
//     }).catch(error => {
//         console.error(error)
//         ret = error;
//     });

//     return ret; 
// }

async function SalvarPrint(idDossie, base64){
    let headers = {
        'chave': '5wVD6J26',
        'Content-Type': 'application/json'
    };    
    let urlApi = `${apiE2ePersistencia.url}/api/upload-base64-to-png`;    
    let ret;    
    let jsonParam = {        
        "base64": base64,        
        "idDossie": idDossie    
    }         
    
    //At request level    
    const agent = new https.Agent({            
        rejectUnauthorized: false    
    });    
    
    await axios.post(urlApi, jsonParam, { httpsAgent: agent, headers: headers}).then(res => {                
        ret = res;    
    }).catch(error => {        
        console.error(error)        
        ret = error;    
    });    
    return ret; 
}