const { Builder, By, Key, until } = require('selenium-webdriver');
const os = require('os');
const fx = require('./comum/funcoes');
var config = require('./config.json');
//const jsonModelo = require('./model/SojEnvioCausas2');
const persist = require('./persistencia/api-e2e-persistencia');

let urlInicialHml = "https://tjbjuridico.santanderbr.pre.corp/JURIDICO_HOM/Login";
let urlInicial = "http://intranet.transacional.santander.br.corp/F6_Portal_Corporativo/Html/F6_Frame_Inicial.htm";
let ambiente = config.ambiente;
//ambiente = "prod";

const entradaDossie1 = require('./src/entradaDossies_1_entrada');
const entradaDossie2 = require('./src/entradaDossies_2_processo');
const entradaDossie3 = require('./src/entradaDossies_3_eventos');
const entradaDossie4 = require('./src/entradaDossies_4_participantes');
const entradaDossie5 = require('./src/entradaDossies_5_documentos');
const entradaDossie6 = require('./src/entradaDossies_6_pedidos');
const entradaDossie7 = require('./src/entradaDossies_7_finalizar');
const entradaEvento = require('./src/entradaEvento');
const entradaDocumento = require('./src/entradaDocumento');


(async() => {   
    let hostname = os.hostname();
    let idDossie = "";
    let tipoCadastro = "";
    let inicio = new Date();
    let fim = "";
    let instancia = fx.guid();    
              
    while(true){ 
        console.log("----------------Inicio do Cadastramento----------------" + new Date()); 
        
        let dossie = null;
        let json = null;
        let chrome = null;
        let driver = null;

        try{             
            dossie = await persist.ObterDossie(hostname, instancia);                
            if(dossie && dossie.data && dossie.data != ""){        
                idDossie = dossie.data.id_iddossie;                  
                tipoCadastro = dossie.data.ds_tipocadastro;

                json = JSON.parse(dossie.data.ds_jsonrecebido);
                
                ({ chrome, driver } = await inicializarChrome(chrome, driver, instancia));                
                await navegacaoInicial(driver); 
                
                switch (tipoCadastro) {        
                    case 'Evento':            
                        await cadastrarEvento(driver, json, idDossie, hostname);
                        break;        
                    case 'Documento':            
                        await cadastrarDocumento(driver, json, idDossie, hostname);      
                        break;        
                    default:            
                        await cadastrarDossie(driver, json, idDossie, hostname);           
                        break;    
                }

                let jsonRetorno = await persist.GerarJsonRetorno(idDossie);
                //await persist.KafkaProducer(JSON.stringify(jsonRetorno.data), "" + json.GestorJuridico.IdServico);
                //await persist.SalvarRetorno(idDossie, JSON.stringify(jsonRetorno.data));
                await fx.sleep(3000);
                                
            } else {
                //quando não encontra registro espera um pouco antes de tentar novamente
                console.log("Não foram encontrados dossies");
                await fx.sleep(60000);
            }          
        } catch (e) {
            fim = new Date();

            if(e.message != "Campo não parametrizado"){
                await persist.RegistrarLog(hostname, idDossie, inicio, fim, "Erro", "0", e.message);
                await fx.sleep(60000);
            }            
            console.log(e);

            let jsonRetorno = await persist.GerarJsonRetorno(idDossie);
              
            //await persist.SalvarRetorno(idDossie, JSON.stringify(jsonRetorno.data)); 
            //await persist.KafkaProducer(JSON.stringify(jsonRetorno.data), "" + json.GestorJuridico.IdServico);
        } finally {                       
            await fecharChrome(driver); 
            console.log("----------------Fim do Cadastramento----------------" + new Date()); 
            await fx.sleep(10000);
        }
    }
})();





async function inicializarChrome(chrome, driver, instancia) {
    try{
        chrome = require('selenium-webdriver/chrome');
        //driver = await new Builder().forBrowser('chrome').build();
        driver = await new Builder().forBrowser('chrome').setChromeOptions(new chrome.Options().setUserPreferences(
            { "download.default_directory": instancia })).build();
        await driver.manage().window().maximize();
        return { chrome, driver };
    } catch(e){
        throw e;
    }
}


async function fecharChrome(driver) {
    try{
        if(driver){
            await driver.close();

            let abas = await driver.getAllWindowHandles();
            await driver.switchTo().window(abas[0]);
            await driver.close();
        }
    } catch(e){
        throw e;
    }
}


async function navegacaoInicial(driver) {
    try{
        if (ambiente == "prod") {
            //navegação inicial produção
            await driver.get(urlInicial);

            await fx.sleep(5000);

            await driver.switchTo().frame(driver.findElement(By.xpath(`//*[@name="menu"]`)));
            await driver.findElement(By.xpath(`//td[contains(text(), "TJ - Jurídico-Novo Benner")]`)).click();

            await fx.sleep(10000);
            await driver.manage().window().maximize();

            let abas = await driver.getAllWindowHandles();
            await driver.switchTo().window(abas[1]);

            //await driver.executeScript('document.querySelector("#menuAtalhos > ul > li:nth-child(9)").className = "dropdown dropdown-extended dropdown-user dropdown-user-wes open"');
            await driver.executeScript(`document.querySelector("#menuAtalhos > ul > li > a > button[class='btn btn-icon-only btn-circle btn-sm usuario']").parentElement.parentElement.className = "dropdown dropdown-extended dropdown-user dropdown-user-wes open"`);
        
            //await driver.executeScript(fx.clickByScript('//*[@id="menuAtalhos"]/ul/li/a/button[@class="btn btn-icon-only btn-circle btn-sm usuario"]/../..'));
            await driver.findElement(By.xpath(`//*[contains(text(), "Responsável pelo cadastro")]`)).click();
            
            
            //await driver.findElement(By.xpath(`//*[contains(text(), "Responsável pelo cadastro")]`)).click();
            await fx.sleep(3000);
        } else {
            //navegação inicial homologação
            await driver.executeScript("window.open('');");
            let abas = await driver.getAllWindowHandles();
            await driver.switchTo().window(abas[1]);

            await driver.get(urlInicialHml);

            for (a = 0; a <= 20; a++) {
                let inputUser = await driver.findElements(By.xpath(`//*[@id='UserName']`));
                if (inputUser.length > 0) {
                    a = 100;
                }
                await fx.sleep(5000);
            }

            await driver.findElement(By.xpath(`//*[@id='UserName']`)).sendKeys('T689869');
            await driver.findElement(By.xpath(`//*[@id='Password']`)).sendKeys('benner123');
            await driver.findElement(By.xpath(`//*[@id="LoginButton"]`)).click();
            await fx.sleep(5000);
        }
    } catch(e){
        throw e;
    }
}


async function cadastrarDossie(driver, json, idDossie, hostname) {
    try{
        let inicio = "";
        let fim = "";

        await driver.executeScript('document.querySelector("#ctl00_SidebarMenu > li.new-item-open > div > ul").style.display = "flex"');
        await fx.sleep(1000);
        await driver.findElement(By.xpath(`//*[@id="ctl00_SidebarMenu"]/li//a[contains(text(), " Entrada de Dossiês")]`)).click();
        await fx.sleep(5000);

        inicio = new Date();
        await entradaDossie1.Consultar(driver, json, idDossie);
        fim = new Date();
        await persist.RegistrarLog(hostname, idDossie, inicio, fim, "Entrada Cadastrada", "1", null);
        await fx.sleep(5000);

        inicio = new Date();
        await entradaDossie2.Consultar(driver, json, idDossie);
        fim = new Date();
        await persist.RegistrarLog(hostname, idDossie, inicio, fim, "Processo Cadastrado", "1", null);
        await fx.sleep(5000);

        inicio = new Date();
        await entradaDossie3.Consultar(driver, json, idDossie);
        fim = new Date();
        await persist.RegistrarLog(hostname, idDossie, inicio, fim, "Eventos Cadastrados", "1", null);
        await fx.sleep(5000);

        let parts = await jsonParticipantes(json.Participantes);
        inicio = new Date();
        await entradaDossie4.Consultar(driver, parts, idDossie, ambiente);
        fim = new Date();
        await persist.RegistrarLog(hostname, idDossie, inicio, fim, "Participantes Cadastrados", "1", null);
        await fx.sleep(5000);

        inicio = new Date();
        await entradaDossie5.Consultar(driver, json, idDossie);
        fim = new Date();
        await persist.RegistrarLog(hostname, idDossie, inicio, fim, "Documentos Cadastrados", "1", null);
        await fx.sleep(5000);

        inicio = new Date();
        await entradaDossie6.Consultar(driver, json, idDossie);
        fim = new Date();
        await persist.RegistrarLog(hostname, idDossie, inicio, fim, "Pedidos Cadastrados", "1", null);
        await fx.sleep(5000);

        inicio = new Date();
        await entradaDossie7.Consultar(driver, json, idDossie);
        fim = new Date();
        await persist.RegistrarLog(hostname, idDossie, inicio, fim, "Dossie Cadastrado", "1", null); 
    } catch(e){
        throw e;
    }
}


async function cadastrarEvento(driver, json, idDossie, hostname) {    
    try{
        let inicio = "";
        let fim = "";

        await navegacaoDossie(driver, json.Evento.NumeroUnico, idDossie);

        await driver.findElement(By.xpath(`//div[@id="ctl00_Main_ANDAMENTOS_GRID_toolbar"]/a[@class="btn green btn-new command-action predef-action editable"]`)).click();
        await fx.sleep(5000);

        inicio = new Date();
        await entradaEvento.Consultar(driver, json, idDossie);
        fim = new Date();
        await persist.RegistrarLog(hostname, idDossie, inicio, fim, "Eventos Cadastrados", "1", null);
    } catch(e){
        throw e;
    }
}


async function cadastrarDocumento(driver, json, idDossie, hostname) {    
    try{
        let inicio = "";
        let fim = "";

        await navegacaoDossie(driver, json.Documento.NumeroUnico, idDossie);

        await driver.findElement(By.xpath(`//div[@id="ctl00_Main_DOCUMENTOS_GRID_toolbar"]/a[@class="btn green btn-new command-action predef-action editable"]`)).click();
        await fx.sleep(5000);

        inicio = new Date();
        await entradaDocumento.Consultar(driver, json, idDossie);
        fim = new Date();
        await persist.RegistrarLog(hostname, idDossie, inicio, fim, "Documentos Cadastrados", "1", null);
    } catch(e){
        throw e;
    }
}


async function navegacaoDossie(driver, numeroUnico, idDossie) {
    try{
        await driver.findElement(By.xpath(`//*[@id="topmenu-PR_PROCESSOS_MENU"]/a`)).click();
        await fx.sleep(2000);

        //numeroProcesso    
        await driver.executeScript(fx.clickByScript(`//*[@id="ctl00_Main_WIDGETID_635929640475007316_FilterControl_GERAL"]//div[contains(text(), "Nº processo principal")]/../../following-sibling::span//a`));
        await fx.sleep(2000);
        await driver.switchTo().frame(driver.findElement(By.xpath(`//*[@name="lookupIframe$ctl00_Main_WIDGETID_635929640475007316_FilterControl_GERAL_ctl19_ctl01_select_modal"]`)));
        await fx.sleep(2000);
        await driver.executeScript(fx.clickByScript(`//*[@id="Resultado_FilterControl_SearchField"]`, numeroUnico));
        await driver.executeScript(fx.clickByScript('//*[@id="Resultado_FilterControl_SearchButton"]/i'));
        await fx.sleep(2000);
        let evento = await driver.findElements(By.xpath(`//*[@id="Resultado_SimpleGrid"]/tbody/tr/td/a`));
        if (evento.length == 0) {
            let res = await persist.SalvarCampoNaoParametrizado("Nº processo principal", numeroUnico, idDossie);
            throw new Error('Campo não parametrizado');
        }
        await driver.executeScript(fx.clickByScript(`//*[@id="Resultado_SimpleGrid"]/tbody/tr/td/a`));

        await fx.sleep(2000);
        abas = await driver.getAllWindowHandles();
        await driver.switchTo().window(abas[1]);
        await fx.sleep(1000);

        await driver.findElement(By.xpath(`//*[@id="ctl00_Main_WIDGETID_635929640475007316_FilterControl_FilterButton"]`)).click();
        await fx.sleep(2000);

        let processo = await driver.findElements(By.xpath(`//*[@id="ctl00_Main_WIDGETID_635929640475007316_SimpleGrid"]/tbody/tr/td/a[contains(text(), "` + numeroUnico + `")]`));
        if (processo.length == 0) {
            let res = await persist.SalvarCampoNaoParametrizado("Nº processo principal", numeroUnico, idDossie);
            throw new Error('Campo não parametrizado');
        }
        await driver.executeScript(fx.clickByScript(`//*[@id="ctl00_Main_WIDGETID_635929640475007316_SimpleGrid"]/tbody/tr/td/a[contains(text(), "` + numeroUnico + `")]`));
        await fx.sleep(5000);
    } catch(e){
        throw e;
    }
}


async function jsonParticipantes(Participantes){
    try{
        var res = [];   

        for(a = 0; a < Participantes.Clientes.length; a++){
            var item = Participantes.Clientes[a];
            item.Classe = "CLIENTE";
            item.Classificacao = "Cliente";
            res.push(item);
        }

        for(a = 0; a < Participantes.Adversos.length; a++){
            var item = Participantes.Adversos[a];
            item.Classe = "ADVERSO";
            item.Classificacao = "Adverso";
            res.push(item);
        }

        for(a = 0; a < Participantes.Terceiros.length; a++){
            var item = Participantes.Terceiros[a];
            item.Classe = "TERCEIRO";
            item.Classificacao = "Terceiro";
            res.push(item);
        }

        for(a = 0; a < Participantes.Advogados.length; a++){
            var item = Participantes.Advogados[a];            
            let tp = Participantes.Advogados[a].Condicao.split(" ");
            item.Classe = "advogado";
            item.Subclasse = tp[1];
            item.Classificacao = Participantes.Advogados[a].Condicao;
            res.push(item);
        }

        return res;
    } catch(e){
        throw e;
    }   
}