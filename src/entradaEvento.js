const { Builder, By, Key, until } = require('selenium-webdriver');
const fx = require('../comum/funcoes');
const persist = require('../persistencia/api-e2e-persistencia');

module.exports = {
  async Consultar(driver, json, idDossie) {
    try {
      await driver.switchTo().frame(driver.findElement(By.xpath(`//*[@id="ModalCommand_modal"]/div/div/div/iframe`)));

      //data
      await driver.executeScript(fx.clickByScript(`//input[@id="ctl00_Main_PR_PROCESSOEVENTOS_FORM_PageControl_EVENTO_EVENTO_DATAEVENTO_DATE"]`, json.Evento.Data));        
      await fx.sleep(1000);
      
      //hora
      await driver.executeScript(fx.clickByScript(`//input[@id="ctl00_Main_PR_PROCESSOEVENTOS_FORM_PageControl_EVENTO_EVENTO_HORAEVENTO_TIME"]`, json.Evento.Hora));
      await fx.sleep(1000);
      
      //evento
      if (json.Evento.DsEvento != null) {
        await driver.executeScript(fx.clickByScript(`//div[@id="ctl00_Main_PR_PROCESSOEVENTOS_FORM_PageControl_EVENTO_EVENTO"]/div/div/div/label/div/div[contains(text(), "Evento")]/../../following-sibling::span//a[@class="btn btn-default"]`));
        await fx.sleep(2000);
        await driver.switchTo().frame(driver.findElement(By.xpath(`//*[@name="lookupIframe$ctl00_Main_PR_PROCESSOEVENTOS_FORM_PageControl_EVENTO_EVENTO_ctl23_ctl01_select_modal"]`)));
        await fx.sleep(2000);
        await driver.executeScript(fx.clickByScript(`//*[@id="Resultado_FilterControl_SearchField"]`, json.Evento.DsEvento));
        await driver.executeScript(fx.clickByScript('//*[@id="Resultado_FilterControl_SearchButton"]/i'));
        await fx.sleep(2000);
        let evento = await driver.findElements(By.xpath(`//*[@id="Resultado_SimpleGrid"]/tbody/tr/td/a`));
        if (evento.length == 0) {
          let res = await persist.SalvarCampoNaoParametrizado("Evento", json.Evento.DsEvento, idDossie);
          throw new Error('Campo não parametrizado');
        }
        await driver.executeScript(fx.clickByScript(`//*[@id="Resultado_SimpleGrid"]/tbody/tr/td/a`));
  
        await fx.sleep(2000);
        abas = await driver.getAllWindowHandles();
        await driver.switchTo().window(abas[1]);
        await fx.sleep(1000);

        await driver.switchTo().frame(driver.findElement(By.xpath(`//*[@id="ModalCommand_modal"]/div/div/div/iframe`)));
        await fx.sleep(1000);
      }
    
      if (json.Evento.Observacoes != null) {
        await driver.executeScript(fx.clickByScript(`//textarea[@id="ctl00_Main_PR_PROCESSOEVENTOS_FORM_PageControl_EVENTO_EVENTO_OBSERVACOES"]`, json.Evento.Observacoes));
      }

      //salvar    
      await driver.executeScript(fx.clickByScript(`//*[@id="ctl00_Main_PR_PROCESSOEVENTOS_FORM_FormContainer"]/div//*[@class="btn blue btn-save command-action predef-action editable"]`));
      await fx.sleep(4000);

      //verifico se houve erro ao Salvar
      let msgErro = await driver.findElements(By.xpath(`//*[@id="ctl00_Main_PR_PROCESSOEVENTOS_FORM_MsgUser_message"]`));
      if (msgErro.length > 0) {
        let msg = await driver.findElement(By.xpath(`//*[@id="ctl00_Main_PR_PROCESSOEVENTOS_FORM_MsgUser_message"]`)).getAttribute("innerHTML");
        throw new Error('Erro ao cadastrar o registro na tela de Evento - Msg:' + msg);
      }

    } catch (e) {
        throw e;
    }
  }
}