const { Builder, By, Key, until } = require('selenium-webdriver');
const fx = require('../comum/funcoes');
const persist = require('../persistencia/api-e2e-persistencia');

module.exports = {
  async Consultar(driver, json, idDossie) {
    try { 

      for(let a = 0; a < json.Eventos.length; a++){
        
        //data
        await driver.executeScript(fx.clickByScript(`//input[@id="ctl00_Main_FORM_EP_PROCESSOEVENTOS_FORM_PageControl_EVENTO_EVENTO_DATAEVENTO_DATE"]`, json.Eventos[a].DataEvento));        
        //await fx.sleep(1000);
        
        //hora
        await driver.executeScript(fx.clickByScript(`//input[@id="ctl00_Main_FORM_EP_PROCESSOEVENTOS_FORM_PageControl_EVENTO_EVENTO_HORAEVENTO_TIME"]`, json.Eventos[a].Hora));
        //await fx.sleep(1000);
        
        //evento
        if (json.Eventos[a] != null) {
          await driver.executeScript(fx.clickByScript(`//div[contains(text(), "Evento")]/../../following-sibling::span//a`));
          //await fx.sleep(2000);
          await driver.switchTo().frame(driver.findElement(By.xpath(`//*[@name="lookupIframe$ctl00_Main_FORM_EP_PROCESSOEVENTOS_FORM_PageControl_EVENTO_EVENTO_ctl32_ctl01_select_modal"]`)));
          //await fx.sleep(2000);
          await driver.executeScript(fx.clickByScript(`//*[@id="Resultado_FilterControl_SearchField"]`, json.Eventos[a].Evento));
          await driver.executeScript(fx.clickByScript('//*[@id="Resultado_FilterControl_SearchButton"]/i'));
          await fx.sleep(2000);
          let evento = await driver.findElements(By.xpath(`//*[@id="Resultado_SimpleGrid"]/tbody/tr/td/a`));
          if (evento.length == 0) {
            let res = await persist.SalvarCampoNaoParametrizado("Evento", json.Eventos[a].Evento, idDossie);
            throw new Error('Campo não parametrizado');
          }
          await driver.executeScript(fx.clickByScript(`//*[@id="Resultado_SimpleGrid"]/tbody/tr/td/a`));
    
          await fx.sleep(1000);
          abas = await driver.getAllWindowHandles();
          await driver.switchTo().window(abas[1]);
          //await fx.sleep(1000);
        }

        //se não for o último evento
        if(a < json.Eventos.length -1){
          //salvar/Novo    
          await driver.executeScript(fx.clickByScript(`//*[@id="ctl00_Main_FORM_EP_PROCESSOEVENTOS_FORM_FormContainer"]/div//*[@class="btn green btn-savenew command-action predef-action editable"]`));
          await fx.sleep(2000);

          //verifico se houve erro ao Salvar
          let msgErro = await driver.findElements(By.xpath(`//*[@id="ctl00_Main_FORM_EP_PROCESSOEVENTOS_FORM_MsgUser"]`));
          if (msgErro.length > 0) {
            let msg = await driver.findElement(By.xpath(`//*[@id="ctl00_Main_FORM_EP_PROCESSOEVENTOS_FORM_MsgUser_message"]`)).getAttribute("innerHTML");
            throw new Error('Erro ao cadastrar o registro na tela de Evento - Msg:' + msg);
          }
        }        
      };
      
      //salvar    
      await driver.executeScript(fx.clickByScript(`//*[@id="ctl00_Main_FORM_EP_PROCESSOEVENTOS_FORM_FormContainer"]/div//*[@class="btn blue btn-save command-action predef-action editable"]`));
      await fx.sleep(2000);

      //verifico se houve erro ao Salvar
      let msgErro = await driver.findElements(By.xpath(`//*[@id="ctl00_Main_FORM_EP_PROCESSOEVENTOS_FORM_MsgUser"]`));
      if (msgErro.length > 0) {
        let msg = await driver.findElement(By.xpath(`//*[@id="ctl00_Main_FORM_EP_PROCESSOEVENTOS_FORM_MsgUser_message"]`)).getAttribute("innerHTML");
        throw new Error('Erro ao cadastrar o registro na tela de Evento - Msg:' + msg);
      }

    } catch (e) {
        console.log(e);
    }
  }
}