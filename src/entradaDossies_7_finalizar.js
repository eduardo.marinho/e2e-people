const { Builder, By, Key, until } = require('selenium-webdriver');
const path = require('path');
const fx = require('../comum/funcoes');
const persist = require('../persistencia/api-e2e-persistencia');

module.exports = {
  async Consultar(driver, json, idDossie) {
    await driver.executeScript(fx.clickByScript('//ul[@class="nav nav-pills nav-justified steps"]/li/a/span[contains(text(), "Finalizar")]'));

    await fx.sleep(2000);
    let base64Data = null;
    await driver.takeScreenshot().then(function(data){
      base64Data = data.replace(/^data:image\/png;base64,/,"");
    });
    await persist.SalvarPrint(idDossie, base64Data);

    let msg = await driver.findElements(By.xpath(`//*[@id="ctl00_Main_PR_PROCESSOS_FORM_MsgUser_message"]`));
    if (msg.length > 0) {
      let msg = await driver.findElement(By.xpath(`//*[@id="ctl00_Main_PR_PROCESSOS_FORM_MsgUser_message"]`)).getAttribute("innerHTML");
      
      if(msg.includes("Cadastro finalizado com sucesso. Pasta criada com o identificador")){
        let numDossie = await driver.findElement(By.xpath(`//*[@id="ctl00_Main_PR_PROCESSOS_FORM_MsgUser_message"]/a`)).getAttribute("innerHTML"); 
        await persist.SalvarNuDossie(idDossie, numDossie);    
        //await fx.sleep(2000);       
      } else if(msg.includes("ExecutaDistribuicao() - ExecutaDistribuicao() - Não foi possível realizar a distribuição automática.ExecutaDistribuicaoCredenciado() - InserirParticipanteCentralizador() - InserirParticipante() - Já existe um registro com estes valores para o campo Processo")){
        let numDossie = "000"; 
        await persist.SalvarNuDossie(idDossie, numDossie);    
        //await fx.sleep(2000);       
      }else {
        throw new Error('Erro ao cadastrar o registro na tela de Finalizar - Msg:' + msg);
      }      
    } else {
      throw new Error('Erro ao cadastrar o registro na tela de Finalizar - Msg: Não foi possível obter o número do dossie');
    }   
  }
}