const { Builder, By, Key, until } = require('selenium-webdriver');
//const path = require('path');
//const funcoes = require('../../comum/funcoes');
const fx = require('../comum/funcoes');
const persist = require('../persistencia/api-e2e-persistencia');

module.exports = {
  async Consultar(driver, json, idDossie) {

    //coligada
    if (json.EntradaDossie.Coligada != null) {
      await driver.executeScript(fx.clickByScript('//div[contains(text(), "Coligada")]/../../following-sibling::span//a'));
      //await fx.sleep(2000);
      await driver.switchTo().frame(driver.findElement(By.xpath(`//*[@name="lookupIframe$ctl00_Main_EP_PROCESSOS_WIDGET_FORM_PageControl_GERAL_GERAL_ctl04_ctl01_select_modal"]`)));
      //await fx.sleep(2000);
      await driver.executeScript(fx.clickByScript('//*[@id="Resultado_FilterControl_SearchField"]', json.EntradaDossie.Coligada));
      await driver.executeScript(fx.clickByScript('//*[@id="Resultado_FilterControl_SearchButton"]/i'));
      await fx.sleep(2000);
      let coligada = await driver.findElements(By.xpath(`//*[@id="Resultado_SimpleGrid"]/tbody/tr/td[1]/a`));
      if (coligada.length == 0) {
        let res = await persist.SalvarCampoNaoParametrizado("Coligada", json.EntradaDossie.Coligada, idDossie);
        throw new Error('Campo não parametrizado');
      }
      await driver.executeScript(fx.clickByScript(`//*[@id="Resultado_SimpleGrid"]/tbody/tr/td[1]/a`));

      await fx.sleep(1000);
      abas = await driver.getAllWindowHandles();
      await driver.switchTo().window(abas[1]);
      //await fx.sleep(1000);
    }

    // Data do fato
    await driver.executeScript(fx.clickByScript(`//input[@id="ctl00_Main_EP_PROCESSOS_WIDGET_FORM_PageControl_GERAL_GERAL_K9_DATAFATO_DATE"]`, json.EntradaDossie.DataFato));
     
    //departamento
    if (json.EntradaDossie.Departamento != null) {
      await driver.executeScript(fx.clickByScript('//div[@title="DEPARTAMENTO>PR_DEPARTAMENTOS.NOME"]/../../following-sibling::span//a'));
      //await fx.sleep(2000);
      await driver.switchTo().frame(driver.findElement(By.xpath(`//*[@name="lookupIframe$ctl00_Main_EP_PROCESSOS_WIDGET_FORM_PageControl_GERAL_GERAL_ctl31_ctl01_select_modal"]`)));
      //await fx.sleep(2000);
      await driver.executeScript(fx.clickByScript('//*[@id="Resultado_FilterControl_SearchField"]', json.EntradaDossie.Departamento));
      await driver.executeScript(fx.clickByScript('//*[@id="Resultado_FilterControl_SearchButton"]/i'));
      await fx.sleep(2000);
      let departamento = await driver.findElements(By.xpath(`//*[@id='Resultado_SimpleGrid']/tbody/tr/td[1]/a`));
      if (departamento.length == 0) {
        let res = await persist.SalvarCampoNaoParametrizado("Departamento", json.EntradaDossie.Departamento, idDossie);
        throw new Error('Campo não parametrizado');
      }
      await driver.executeScript(fx.clickByScript(`//*[@id="Resultado_SimpleGrid"]/tbody/tr/td[1]/a`));

      await fx.sleep(1000);
      abas = await driver.getAllWindowHandles();
      await driver.switchTo().window(abas[1]);
      //await fx.sleep(1000);
    }

    //categoria
    if (json.EntradaDossie.Categoria != null) {
      await driver.executeScript(fx.clickByScript(`//div[contains(text(), "Categoria")]/../../following-sibling::span//a`));
      //await fx.sleep(2000);
      await driver.switchTo().frame(driver.findElement(By.xpath(`//*[@name="lookupIframe$ctl00_Main_EP_PROCESSOS_WIDGET_FORM_PageControl_GERAL_GERAL_ctl35_ctl01_select_modal"]`)));
      //await fx.sleep(2000);
      await driver.executeScript(fx.clickByScript(`//*[@id="Resultado_FilterControl_SearchField"]`, json.EntradaDossie.Categoria));
      await driver.executeScript(fx.clickByScript('//*[@id="Resultado_FilterControl_SearchButton"]/i'));
      await fx.sleep(2000);
      let categoria = await driver.findElements(By.xpath(`//*[@id='Resultado_SimpleGrid']/tbody/tr/td[1]/a`));
      if (categoria.length == 0) {
        let res = await persist.SalvarCampoNaoParametrizado("Categoria", json.EntradaDossie.Categoria, idDossie);
        throw new Error('Campo não parametrizado');
      }
      await driver.executeScript(fx.clickByScript(`//*[@id="Resultado_SimpleGrid"]/tbody/tr/td[1]/a`));

      await fx.sleep(1000);
      abas = await driver.getAllWindowHandles();
      await driver.switchTo().window(abas[1]);
      //await fx.sleep(1000);
    }

    //subcategoria
    if (json.EntradaDossie.Subcategoria != null) {
      await driver.executeScript(fx.clickByScript(`//div[contains(text(), "Subcategoria")]/../../following-sibling::span//a`));
      //await fx.sleep(2000);
      await driver.switchTo().frame(driver.findElement(By.xpath(`//*[@name="lookupIframe$ctl00_Main_EP_PROCESSOS_WIDGET_FORM_PageControl_GERAL_GERAL_ctl39_ctl01_select_modal"]`)));
      //await fx.sleep(2000);
      await driver.executeScript(fx.clickByScript(`//*[@id="Resultado_FilterControl_SearchField"]`, json.EntradaDossie.Subcategoria));
      await driver.executeScript(fx.clickByScript('//*[@id="Resultado_FilterControl_SearchButton"]/i'));
      await fx.sleep(2000);
      let subcategoria = await driver.findElements(By.xpath(`//*[@id='Resultado_SimpleGrid']/tbody/tr/td[1]/a`));
      if (subcategoria.length == 0) {
        let res = await persist.SalvarCampoNaoParametrizado("Subcategoria", json.EntradaDossie.Subcategoria, idDossie);
        throw new Error('Campo não parametrizado');
      }      
      await driver.executeScript(fx.clickByScript(`//*[@id="Resultado_SimpleGrid"]/tbody/tr/td[1]/a`));

      await fx.sleep(1000);
      abas = await driver.getAllWindowHandles();
      await driver.switchTo().window(abas[1]);
      //await fx.sleep(1000);
    }

    //tipoAcao
    if (json.EntradaDossie.TipoAcao != null) {
      await driver.executeScript(fx.clickByScript(`//div[contains(text(), "Tipo de ação")]/../../following-sibling::span//a`));
      //await fx.sleep(2000);
      await driver.switchTo().frame(driver.findElement(By.xpath(`//*[@name="lookupIframe$ctl00_Main_EP_PROCESSOS_WIDGET_FORM_PageControl_GERAL_GERAL_ctl43_ctl01_select_modal"]`)));
      //await fx.sleep(2000);
      await driver.executeScript(fx.clickByScript(`//*[@id="Resultado_FilterControl_SearchField"]`, json.EntradaDossie.TipoAcao));
      await driver.executeScript(fx.clickByScript('//*[@id="Resultado_FilterControl_SearchButton"]/i'));
      await fx.sleep(2000);
      let tipoAcao = await driver.findElements(By.xpath(`//*[@id='Resultado_SimpleGrid']/tbody/tr/td[1]/a`));
      if (tipoAcao.length == 0) {
        let res = await persist.SalvarCampoNaoParametrizado("Tipo de ação", json.EntradaDossie.TipoAcao, idDossie);
        throw new Error('Campo não parametrizado');
      }
      await driver.executeScript(fx.clickByScript(`//*[@id="Resultado_SimpleGrid"]/tbody/tr/td[1]/a`));

      await fx.sleep(1000);
      abas = await driver.getAllWindowHandles();
      await driver.switchTo().window(abas[1]);
      //await fx.sleep(1000);
    }

    //objetoPadrao
    if (json.EntradaDossie.ObjetoPadrao != null) {
      await driver.executeScript(fx.clickByScript(`//div[contains(text(), "Objeto padrão")]/../../following-sibling::span//a`));
      //await fx.sleep(2000);
      await driver.switchTo().frame(driver.findElement(By.xpath(`//*[@name="lookupIframe$ctl00_Main_EP_PROCESSOS_WIDGET_FORM_PageControl_GERAL_GERAL_ctl47_ctl01_select_modal"]`)));
      //await fx.sleep(2000);
      await driver.executeScript(fx.clickByScript(`//*[@id="Resultado_FilterControl_SearchField"]`, json.EntradaDossie.ObjetoPadrao));
      await driver.executeScript(fx.clickByScript('//*[@id="Resultado_FilterControl_SearchButton"]/i'));
      await fx.sleep(2000);
      let objetoPadrao = await driver.findElements(By.xpath(`//*[@id='Resultado_SimpleGrid']/tbody/tr/td[1]/a`));
      if (objetoPadrao.length == 0) {
        let res = await persist.SalvarCampoNaoParametrizado("Objeto padrão", json.EntradaDossie.ObjetoPadrao, idDossie);
        throw new Error('Campo não parametrizado');
      }
      await driver.executeScript(fx.clickByScript(`//*[@id="Resultado_SimpleGrid"]/tbody/tr/td[1]/a`));
      
      await fx.sleep(1000);
      abas = await driver.getAllWindowHandles();
      await driver.switchTo().window(abas[1]);
      //await fx.sleep(1000);
    }

    //assunto
    if (json.EntradaDossie.Assunto != null) {
      await driver.executeScript(fx.clickByScript(`//div[contains(text(), "Assunto")]/../../following-sibling::span//a`));
      //await fx.sleep(2000);
      await driver.switchTo().frame(driver.findElement(By.xpath(`//*[@name="lookupIframe$ctl00_Main_EP_PROCESSOS_WIDGET_FORM_PageControl_GERAL_GERAL_ctl51_ctl01_select_modal"]`)));
      //await fx.sleep(2000);
      await driver.executeScript(fx.clickByScript(`//*[@id="Resultado_FilterControl_SearchField"]`, json.EntradaDossie.Assunto));
      await driver.executeScript(fx.clickByScript('//*[@id="Resultado_FilterControl_SearchButton"]/i'));
      await fx.sleep(2000);
      let assunto = await driver.findElements(By.xpath(`//*[@id='Resultado_SimpleGrid']/tbody/tr/td[1]/a`));
      if (assunto.length == 0) {
        let res = await persist.SalvarCampoNaoParametrizado("Assunto", json.EntradaDossie.Assunto, idDossie);
        throw new Error('Campo não parametrizado');
      }
      await driver.executeScript(fx.clickByScript(`//*[@id="Resultado_SimpleGrid"]/tbody/tr/td[1]/a`));

      await fx.sleep(1000);
      abas = await driver.getAllWindowHandles();
      await driver.switchTo().window(abas[1]);
      //await fx.sleep(1000);
    }

    //natureza
    if (json.EntradaDossie.Natureza != null) {
      await driver.executeScript(fx.clickByScript(`//div[contains(text(), "Natureza")]/../../following-sibling::span//a`));
      //await fx.sleep(2000);
      await driver.switchTo().frame(driver.findElement(By.xpath(`//*[@name="lookupIframe$ctl00_Main_EP_PROCESSOS_WIDGET_FORM_PageControl_GERAL_GERAL_ctl55_ctl01_select_modal"]`)));
      //await fx.sleep(2000);
      await driver.executeScript(fx.clickByScript(`//*[@id="Resultado_FilterControl_SearchField"]`, json.EntradaDossie.Natureza));
      await driver.executeScript(fx.clickByScript('//*[@id="Resultado_FilterControl_SearchButton"]/i'));
      await fx.sleep(2000);
      let natureza = await driver.findElements(By.xpath(`//*[@id='Resultado_SimpleGrid']/tbody/tr/td[1]/a`));
      if (natureza.length == 0) {
        let res = await persist.SalvarCampoNaoParametrizado("Natureza", json.EntradaDossie.Natureza, idDossie);
        throw new Error('Campo não parametrizado');
      }
      await driver.executeScript(fx.clickByScript(`//*[@id="Resultado_SimpleGrid"]/tbody/tr/td[1]/a`));

      await fx.sleep(1000);
      abas = await driver.getAllWindowHandles();
      await driver.switchTo().window(abas[1]);
      //await fx.sleep(1000);
    }

    //bancoEmpresaOrigem
    if (json.EntradaDossie.BancoEmpresaOrigem != null) {
      await driver.executeScript(fx.clickByScript(`//div[contains(text(), "Banco/Empresa origem")]/../../following-sibling::span//a`));
      //await fx.sleep(2000);
      await driver.switchTo().frame(driver.findElement(By.xpath(`//*[@name="lookupIframe$ctl00_Main_EP_PROCESSOS_WIDGET_FORM_PageControl_GERAL_GERAL_ctl82_ctl01_select_modal"]`)));
      //await fx.sleep(2000);
      await driver.executeScript(fx.clickByScript(`//*[@id="Resultado_FilterControl_SearchField"]`, json.EntradaDossie.BancoEmpresaOrigem));
      await driver.executeScript(fx.clickByScript('//*[@id="Resultado_FilterControl_SearchButton"]/i'));
      await fx.sleep(2000);
      let bancoEmpresaOrigem = await driver.findElements(By.xpath(`//*[@id='Resultado_SimpleGrid']/tbody/tr/td[1]/a`));
      if (bancoEmpresaOrigem.length == 0) {
        let res = await persist.SalvarCampoNaoParametrizado("Banco/Empresa origem", json.EntradaDossie.BancoEmpresaOrigem, idDossie);
        throw new Error('Campo não parametrizado');
      }
      await driver.executeScript(fx.clickByScript(`//*[@id="Resultado_SimpleGrid"]/tbody/tr/td[1]/a`));

      await fx.sleep(1000);
      abas = await driver.getAllWindowHandles();
      await driver.switchTo().window(abas[1]);
      //await fx.sleep(1000);
    }    
    
    //canalContratacao
    if (json.EntradaDossie.CanalContratacao != null) {
      await driver.executeScript(fx.clickByScript(`//div[contains(text(), "Canal de contratação")]/../../following-sibling::span//a`));
      //await fx.sleep(2000);
      await driver.switchTo().frame(driver.findElement(By.xpath(`//*[@name="lookupIframe$ctl00_Main_EP_PROCESSOS_WIDGET_FORM_PageControl_GERAL_GERAL_ctl94_ctl01_select_modal"]`)));
      //await fx.sleep(2000);
      await driver.executeScript(fx.clickByScript(`//*[@id="Resultado_FilterControl_SearchField"]`, json.EntradaDossie.CanalContratacao));
      await driver.executeScript(fx.clickByScript('//*[@id="Resultado_FilterControl_SearchButton"]/i'));
      await fx.sleep(2000);
      let canalContratacao = await driver.findElements(By.xpath(`//*[@id='Resultado_SimpleGrid']/tbody/tr/td[1]/a`));
      if (canalContratacao.length == 0) {
        let res = await persist.SalvarCampoNaoParametrizado("Canal de contratação", json.EntradaDossie.CanalContratacao, idDossie);
        throw new Error('Campo não parametrizado');
      }
      await driver.executeScript(fx.clickByScript(`//*[@id="Resultado_SimpleGrid"]/tbody/tr/td[1]/a`));

      await fx.sleep(1000);
      abas = await driver.getAllWindowHandles();
      await driver.switchTo().window(abas[1]);
      //await fx.sleep(1000);
    }

    //tipoCaptura
    if (json.EntradaDossie.TipoCaptura != null) {
      await driver.executeScript(fx.clickByScript(`//div[contains(text(), "Tipo de captura")]/../../following-sibling::span//a`));
      //await fx.sleep(2000);
      await driver.switchTo().frame(driver.findElement(By.xpath(`//*[@name="lookupIframe$ctl00_Main_EP_PROCESSOS_WIDGET_FORM_PageControl_GERAL_GERAL_ctl98_ctl01_select_modal"]`)));
      //await fx.sleep(2000);
      await driver.executeScript(fx.clickByScript(`//*[@id="Resultado_FilterControl_SearchField"]`, json.EntradaDossie.TipoCaptura));
      await driver.executeScript(fx.clickByScript('//*[@id="Resultado_FilterControl_SearchButton"]/i'));
      await fx.sleep(2000);
      let tipoCaptura = await driver.findElements(By.xpath(`//*[@id='Resultado_SimpleGrid']/tbody/tr/td[1]/a`));
      if (tipoCaptura.length == 0) {
        let res = await persist.SalvarCampoNaoParametrizado("Tipo de captura", json.EntradaDossie.TipoCaptura, idDossie);
        throw new Error('Campo não parametrizado');
      }
      await driver.executeScript(fx.clickByScript(`//*[@id="Resultado_SimpleGrid"]/tbody/tr/td[1]/a`));

      await fx.sleep(1000);
      abas = await driver.getAllWindowHandles();
      await driver.switchTo().window(abas[1]);
      //await fx.sleep(1000);
    }

    //valorCausa
    if (json.EntradaDossie.ValorCausa != null) {
      await driver.executeScript(fx.clickByScript(`//*[@id="ctl00_Main_EP_PROCESSOS_WIDGET_FORM_PageControl_GERAL_GERAL_K9_VALORAJUIZADO"]`, json.EntradaDossie.ValorCausa));
    }  
    
    //danos
    if (json.EntradaDossie.Danos != null && json.EntradaDossie.Danos != "") {
      await driver.findElement(By.xpath(`//select[@id="ctl00_Main_EP_PROCESSOS_WIDGET_FORM_PageControl_GERAL_GERAL_K9_DANOSselect"]/option[text() = "` + json.EntradaDossie.Danos + `"]`)).click();
      await fx.sleep(2000);      
    }

    //classificacaoProcesso
    if (json.EntradaDossie.ClassificacaoProcesso != null) {
      await driver.executeScript(fx.clickByScript('//div[contains(text(), "Classificação do processo")]/../../following-sibling::span//a'));
      //await fx.sleep(2000);
      await driver.switchTo().frame(driver.findElement(By.xpath(`//*[@name="lookupIframe$ctl00_Main_EP_PROCESSOS_WIDGET_FORM_PageControl_GERAL_GERAL_ctl121_ctl01_select_modal"]`)));
      //await fx.sleep(2000);
      await driver.executeScript(fx.clickByScript('//*[@id="Resultado_FilterControl_SearchField"]', json.EntradaDossie.ClassificacaoProcesso));
      await driver.executeScript(fx.clickByScript('//*[@id="Resultado_FilterControl_SearchButton"]/i'));
      await fx.sleep(2000);
      let classificacaoProcesso = await driver.findElements(By.xpath(`//*[@id="Resultado_SimpleGrid"]/tbody/tr/td[1]/a`));
      if (classificacaoProcesso.length == 0) {
        let res = await persist.SalvarCampoNaoParametrizado("Classificação do processo", json.EntradaDossie.ClassificacaoProcesso, idDossie);
        throw new Error('Campo não parametrizado');
      }
      await driver.executeScript(fx.clickByScript(`//*[@id="Resultado_SimpleGrid"]/tbody/tr/td[1]/a`));

      await fx.sleep(1000);
      abas = await driver.getAllWindowHandles();
      await driver.switchTo().window(abas[1]);
      //await fx.sleep(1000);
    }

    //classificacaoContingencia
    if (json.EntradaDossie.ClassificacaoContigencia != null) {
      await driver.executeScript(fx.clickByScript('//div[contains(text(), "Classificação da contingência")]/../../following-sibling::span//a'));
      //await fx.sleep(2000);
      await driver.switchTo().frame(driver.findElement(By.xpath(`//*[@name="lookupIframe$ctl00_Main_EP_PROCESSOS_WIDGET_FORM_PageControl_GERAL_GERAL_ctl125_ctl01_select_modal"]`)));
      //await fx.sleep(2000);
      await driver.executeScript(fx.clickByScript('//*[@id="Resultado_FilterControl_SearchField"]', json.EntradaDossie.ClassificacaoContigencia));
      await driver.executeScript(fx.clickByScript('//*[@id="Resultado_FilterControl_SearchButton"]/i'));
      await fx.sleep(2000);
      let classificacaoContingencia = await driver.findElements(By.xpath(`//*[@id="Resultado_SimpleGrid"]/tbody/tr/td[1]/a`));
      if (classificacaoContingencia.length == 0) {
        let res = await persist.SalvarCampoNaoParametrizado("Classificação da contingência", json.EntradaDossie.ClassificacaoContigencia, idDossie);
        throw new Error('Campo não parametrizado');
      }
      await driver.executeScript(fx.clickByScript(`//*[@id="Resultado_SimpleGrid"]/tbody/tr/td[1]/a`));

      await fx.sleep(1000);
      abas = await driver.getAllWindowHandles();
      await driver.switchTo().window(abas[1]);
      //await fx.sleep(1000);
    }

    //tipoDesligamento
    if (json.EntradaDossie.TipoDesligamento != null) {
      await driver.executeScript(fx.clickByScript(`//div[contains(text(), "Tipo desligamento")]/../../following-sibling::span//a`));
      //await fx.sleep(2000);
      await driver.switchTo().frame(driver.findElement(By.xpath(`//*[@name="lookupIframe$ctl00_Main_EP_PROCESSOS_WIDGET_FORM_PageControl_GERAL_GERAL_ctl133_ctl01_select_modal"]`)));
      //await fx.sleep(2000);
      await driver.executeScript(fx.clickByScript(`//*[@id="Resultado_FilterControl_SearchField"]`, json.EntradaDossie.TipoDesligamento));
      await driver.executeScript(fx.clickByScript('//*[@id="Resultado_FilterControl_SearchButton"]/i'));
      await fx.sleep(2000);
      let tipoDesligamento = await driver.findElements(By.xpath(`//*[@id='Resultado_SimpleGrid']/tbody/tr/td[1]/a`));
      if (tipoDesligamento.length == 0) {
        let res = await persist.SalvarCampoNaoParametrizado("Tipo desligamento", json.EntradaDossie.TipoDesligamento, idDossie);
        throw new Error('Campo não parametrizado');
      }
      await driver.executeScript(fx.clickByScript(`//*[@id="Resultado_SimpleGrid"]/tbody/tr/td[1]/a`));

      await fx.sleep(1000);
      abas = await driver.getAllWindowHandles();
      await driver.switchTo().window(abas[1]);
      //await fx.sleep(1000);
    }

    //pastaAntiga
    if (json.EntradaDossie.PastaAntiga != null) {
      await driver.executeScript(fx.clickByScript(`//*[@id="ctl00_Main_EP_PROCESSOS_WIDGET_FORM_PageControl_GERAL_GERAL_K9_NRPROCESSOANTIGO"]`, json.EntradaDossie.PastaAntiga));
    }  
    
    //relevante
    if (json.EntradaDossie.Relevante != null && json.EntradaDossie.Relevante.toUpperCase() == "SIM") {
      await driver.executeScript(fx.clickByScript(`//div[contains(text(), "Processo relevante")]/..//div/div/label/label[text()="Sim"]`));
      await fx.sleep(1000);

      //motivoRelevancia
      if (json.EntradaDossie.MotivoRelevancia != null) {
        await driver.executeScript(fx.clickByScript(`//div[contains(text(), "Motivo relevância")]/../../following-sibling::span//a`));
        //await fx.sleep(2000);
        await driver.switchTo().frame(driver.findElement(By.xpath(`//*[@name="lookupIframe$ctl00_Main_EP_PROCESSOS_WIDGET_FORM_PageControl_GERAL_GERAL_PROCESSORELEVANTE_2_ctl04_ctl01_select_modal"]`)));
        //await fx.sleep(2000);
        await driver.executeScript(fx.clickByScript(`//*[@id="Resultado_FilterControl_SearchField"]`, json.EntradaDossie.MotivoRelevancia));
        await driver.executeScript(fx.clickByScript('//*[@id="Resultado_FilterControl_SearchButton"]/i'));
        await fx.sleep(2000);
        let motivoRelevancia = await driver.findElements(By.xpath(`//*[@id='Resultado_SimpleGrid']/tbody/tr/td[1]/a`));
        if (motivoRelevancia.length == 0) {
          let res = await persist.SalvarCampoNaoParametrizado("Motivo relevância", json.EntradaDossie.MotivoRelevancia, idDossie);
          throw new Error('Campo não parametrizado');
        }
        await driver.executeScript(fx.clickByScript(`//*[@id="Resultado_SimpleGrid"]/tbody/tr/td[1]/a`));
  
        await fx.sleep(1000);
        abas = await driver.getAllWindowHandles();
        await driver.switchTo().window(abas[1]);
        //await fx.sleep(1000);
      }
    }
      
    //dossieUrgente
    if (json.EntradaDossie.DossieUrgente != null) {
      if(json.EntradaDossie.DossieUrgente.toUpperCase() == "NÃO"){
        await driver.executeScript(fx.clickByScript(`//div[contains(text(), "Dossiê urgente")]/..//div/div/label/label[text()="Não"]`));
      }
      if(json.EntradaDossie.DossieUrgente.toUpperCase() == "SIM"){
        await driver.executeScript(fx.clickByScript(`//div[contains(text(), "Dossiê urgente")]/..//div/div/label/label[text()="Sim"]`));
      }
      await fx.sleep(2000);
      abas = await driver.getAllWindowHandles();
      await driver.switchTo().window(abas[1]);
      //await fx.sleep(1000);
    }   

    //salvar
    await driver.executeScript(fx.clickByScript(`//*[@id="ctl00_Main_EP_PROCESSOS_WIDGET_FORM_FormContainer"]/div//*[@class="btn blue btn-save command-action predef-action editable"]`));
    await fx.sleep(2000);

    //verifico se houve erro ao Salvar
    let msgErro = await driver.findElements(By.xpath(`//*[@id="ctl00_Main_EP_PROCESSOS_WIDGET_FORM_MsgUser_message"]`));
    if (msgErro.length > 0) {
      let msg = await driver.findElement(By.xpath(`//*[@id="ctl00_Main_EP_PROCESSOS_WIDGET_FORM_MsgUser_message"]`)).getAttribute("innerHTML");
      throw new Error('Erro ao cadastrar o registro na tela de Entrada - Msg:' + msg);
    }     
  }
}