const { Builder, By, Key, until } = require('selenium-webdriver');
const fx = require('../comum/funcoes');
const persist = require('../persistencia/api-e2e-persistencia');

module.exports = {
  async Consultar(driver, json, idDossie) {  
    
      let dir = await fx.criarPastaGuid(fx.guid());

      for(let a = 0; a < json.Documentos.length; a++){

        // Data
        await driver.executeScript(fx.clickByScript(`//input[@id="ctl00_Main_CADASTRO_PROCESSODOCUMENTOS_FO_PageControl_GERAL_GERAL_DATADOCUMENTO_DATE"]`, json.Documentos[a].DataDocumento));
        //await fx.sleep(1000);

        // Seleciona tipo documento
        if (json.Documentos[a] != null) {
          await driver.executeScript(fx.clickByScript(`//div[contains(text(), "Tipo documento")]/../../following-sibling::span//a`));
          //await fx.sleep(2000);
          await driver.switchTo().frame(driver.findElement(By.xpath(`//*[@name="lookupIframe$ctl00_Main_CADASTRO_PROCESSODOCUMENTOS_FO_PageControl_GERAL_GERAL_ctl15_ctl01_select_modal"]`)));
          //await fx.sleep(2000);
          await driver.executeScript(fx.clickByScript(`//*[@id="Resultado_FilterControl_SearchField"]`, json.Documentos[a].TipoDocumento));
          await driver.executeScript(fx.clickByScript('//*[@id="Resultado_FilterControl_SearchButton"]/i'));
          await fx.sleep(2000);
          let documento = await driver.findElements(By.xpath(`//*[@id="Resultado_SimpleGrid"]/tbody/tr/td/a`));          
          if (documento.length == 0) {
            let res = await persist.SalvarCampoNaoParametrizado("Tipo documento", json.Documentos[a].TipoDocumento, idDossie);
            throw new Error('Campo não parametrizado');
          }
          await driver.executeScript(fx.clickByScript(`//*[@id="Resultado_SimpleGrid"]/tbody/tr/td/a`));

          await fx.sleep(1000);
          abas = await driver.getAllWindowHandles();
          await driver.switchTo().window(abas[1]);
          //await fx.sleep(1000);
        }

        // Nome de documento
        await driver.executeScript(fx.clickByScript(`//input[@id="ctl00_Main_CADASTRO_PROCESSODOCUMENTOS_FO_PageControl_GERAL_GERAL_NOME"]`, json.Documentos[a].NomeDocumento));
        //await fx.sleep(1000);
         
        //faz o download do documento
        let ret = await persist.DocumentoDownload(json.Documentos[a].LinkArquivo);
        //let nomeArquivo = "apiPersistencia_"+fx.guid()+"."+jsonParam.extensao;
        //let dir = path.resolve("./downloads");
        let caminhoArquivo = await fx.base64ToFile(json.Documentos[a].NomeDocumento, ret.data,  dir);
        
        // Início de Upload de documentos
        await driver.executeScript(fx.clickByScript('//*[@id="ctl00_Main_CADASTRO_PROCESSODOCUMENTOS_FO_PageControl_GERAL_GERAL"]/div/div[4]/div/span/div/div[1]/button/i'));
        await fx.sleep(2000);
        await driver.switchTo().frame(driver.findElement(By.xpath(`//*[@id="file-uploader_modal"]/div/div/iframe`)));
        await fx.sleep(1000);
        let uploadElement = await driver.findElement(By.xpath(`//input[@id="file"]`));
        //await uploadElement.sendKeys(json.Documentos[a].LinkArquivo);
        await uploadElement.sendKeys(caminhoArquivo);        
        await fx.sleep(1000);
        await driver.executeScript(fx.clickByScript('//*[@id="btnUploadTheFile"]'));
        await fx.sleep(2000);
        abas = await driver.getAllWindowHandles();
        await driver.switchTo().window(abas[1]);
        //await fx.sleep(1000);
        
        // Seleciona pasta virtual
        if (json.Documentos[a] != null) {
          await driver.executeScript(fx.clickByScript(`//div[contains(text(), "Pasta virtual")]/../../following-sibling::span//a`));
          //await fx.sleep(2000);
          await driver.switchTo().frame(driver.findElement(By.xpath(`//*[@name="lookupIframe$ctl00_Main_CADASTRO_PROCESSODOCUMENTOS_FO_PageControl_GERAL_GERAL_ctl36_ctl01_select_modal"]`)));
          //await fx.sleep(2000);
          await driver.executeScript(fx.clickByScript(`//*[@id="Resultado_FilterControl_SearchField"]`, json.Documentos[a].PastaVirtual));
          await driver.executeScript(fx.clickByScript('//*[@id="Resultado_FilterControl_SearchButton"]/i'));
          await fx.sleep(2000);
          let pasta = await driver.findElements(By.xpath(`//*[@id="Resultado_SimpleGrid"]/tbody/tr/td/a`));
          if (pasta.length == 0) {
            let res = await persist.SalvarCampoNaoParametrizado("Pasta virtual", json.Documentos[a].PastaVirtual, idDossie);
            throw new Error('Campo não parametrizado');
          }
          await driver.executeScript(fx.clickByScript(`//*[@id="Resultado_SimpleGrid"]/tbody/tr/td/a`));
    
          await fx.sleep(1000);
          abas = await driver.getAllWindowHandles();
          await driver.switchTo().window(abas[1]);
          //await fx.sleep(1000);
        }

        //se não for o último evento
        if(a < json.Documentos.length -1){
          //salvar/Novo    
          await driver.executeScript(fx.clickByScript(`//*[@id="ctl00_Main_CADASTRO_PROCESSODOCUMENTOS_FO_FormContainer"]/div//*[@class="btn green btn-savenew command-action predef-action editable"]`));
          await fx.sleep(4000);

          //verifico se houve erro ao Salvar
          let msgErro = await driver.findElements(By.xpath(`//*[@id="ctl00_Main_CADASTRO_PROCESSODOCUMENTOS_FO_MsgUser_message"]`));
          if (msgErro.length > 0) {
            let msg = await driver.findElement(By.xpath(`//*[@id="ctl00_Main_CADASTRO_PROCESSODOCUMENTOS_FO_MsgUser_message"]`)).getAttribute("innerHTML");
            throw new Error('Erro ao cadastrar o registro na tela de Documentos - Msg:' + msg);
          }
        }
      };

      //salvar    
      await driver.executeScript(fx.clickByScript(`//*[@id="ctl00_Main_CADASTRO_PROCESSODOCUMENTOS_FO_FormContainer"]/div//*[@class="btn blue btn-save command-action predef-action editable"]`));
      await fx.sleep(4000);

      //verifico se houve erro ao Salvar
      let msgErro = await driver.findElements(By.xpath(`//*[@id="ctl00_Main_CADASTRO_PROCESSODOCUMENTOS_FO_MsgUser_message"]`));
      if (msgErro.length > 0) {
        let msg = await driver.findElement(By.xpath(`//*[@id="ctl00_Main_CADASTRO_PROCESSODOCUMENTOS_FO_MsgUser_message"]`)).getAttribute("innerHTML");
        await fx.removerPastaDownload(dir);
        throw new Error('Erro ao cadastrar o registro na tela de Documentos - Msg:' + msg);
      }   
      
      await fx.removerPastaDownload(dir);
  }
}