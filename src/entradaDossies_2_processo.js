const { Builder, By, Key, until } = require('selenium-webdriver');
//const path = require('path');
const fx = require('../comum/funcoes');
const persist = require('../persistencia/api-e2e-persistencia');

module.exports = {
  async Consultar(driver, json, idDossie) {          
      
    let nuControleCadastramento = await driver.findElement(By.xpath(`//*[@id="breadcrumbUpdatePanel"]/div/ul/li[1]/a`)).getAttribute("innerHTML");
    await persist.SalvarNuControleCadastramento(idDossie, nuControleCadastramento);        
    //await fx.sleep(2000);

    //tipoProcesso
      if (json.Processo.Processo != null) {
        await driver.executeScript(fx.clickByScript('//div[contains(text(), "Processo")]/../../following-sibling::span//a'));
        //await fx.sleep(2000);
        await driver.switchTo().frame(driver.findElement(By.xpath(`//*[@name="lookupIframe$ctl00_Main_EP_PROCESSODESDOBRAMENTOS_WID_PageControl_GERAL_GERAL_ctl04_ctl01_select_modal"]`)));
        //await fx.sleep(2000);
        await driver.executeScript(fx.clickByScript('//*[@id="Resultado_FilterControl_SearchField"]', json.Processo.Processo));
        await driver.executeScript(fx.clickByScript('//*[@id="Resultado_FilterControl_SearchButton"]/i'));
        await fx.sleep(2000);
        let processo = await driver.findElements(By.xpath(`//*[@id='Resultado_SimpleGrid']/tbody/tr/td[1]/a`));
        if (processo.length == 0) {
          let res = await persist.SalvarCampoNaoParametrizado("Processo", json.Processo.Processo, idDossie);
          throw new Error('Campo não parametrizado');
        }
        await driver.executeScript(fx.clickByScript(`//*[@id="Resultado_SimpleGrid"]/tbody/tr/td[1]/a`));

        await fx.sleep(1000);
        abas = await driver.getAllWindowHandles();
        await driver.switchTo().window(abas[1]);
        //await fx.sleep(1000);
      }

      //flagPrincipal
      if (json.Processo.Principal != null && json.Processo.Principal.toUpperCase() == "SIM") {
        await driver.executeScript(fx.clickByScript(`//label[contains(text(), "Principal")]/../../label[@class="boolean-control mt-checkbox mt-checkbox-outline"]`));
      }
      
      //natureza
      if (json.Processo.Natureza != null) {
        if(json.Processo.Natureza.toUpperCase() == "ADMINISTRATIVA"){
          await driver.executeScript(fx.clickByScript(`//input[@id="ctl00_Main_EP_PROCESSODESDOBRAMENTOS_WID_PageControl_GERAL_GERAL_GroupRadioButton_NATUREZA_A"]/..`));
        }
        if(json.Processo.Natureza.toUpperCase() == "JUDICIAL"){
          await driver.executeScript(fx.clickByScript(`//input[@id="ctl00_Main_EP_PROCESSODESDOBRAMENTOS_WID_PageControl_GERAL_GERAL_GroupRadioButton_NATUREZA_J"]/..`));
        }        
        await fx.sleep(2000);
      }
            
      //instância
      if (json.Processo.Instancia != null) {
        await driver.executeScript(fx.clickByScript(`//div[contains(text(), "Instância")]/../../following-sibling::span//a`));
        //await fx.sleep(2000);
        await driver.switchTo().frame(driver.findElement(By.xpath(`//*[@name="lookupIframe$ctl00_Main_EP_PROCESSODESDOBRAMENTOS_WID_PageControl_GERAL_GERAL_ctl23_ctl01_select_modal"]`)));
        //await fx.sleep(2000);
        await driver.executeScript(fx.clickByScript(`//*[@id="Resultado_FilterControl_SearchField"]`, json.Processo.Instancia));
        await driver.executeScript(fx.clickByScript('//*[@id="Resultado_FilterControl_SearchButton"]/i'));
        await fx.sleep(2000);
        let instancia = await driver.findElements(By.xpath(`//*[@id='Resultado_SimpleGrid']/tbody/tr/td[1]/a`));
        if (instancia.length == 0) {
          let res = await persist.SalvarCampoNaoParametrizado("Instância", json.Processo.Instancia, idDossie);
          throw new Error('Campo não parametrizado');
        }
        await driver.executeScript(fx.clickByScript(`//*[@id="Resultado_SimpleGrid"]/tbody/tr/td[1]/a`));
  
        await fx.sleep(1000);
        abas = await driver.getAllWindowHandles();
        await driver.switchTo().window(abas[1]);
        //await fx.sleep(1000);
      }
      
      //rito
      if (json.Processo.Rito != null) {
        await driver.executeScript(fx.clickByScript(`//div[contains(text(), "Rito")]/../../following-sibling::span//a`));
        //await fx.sleep(2000);
        await driver.switchTo().frame(driver.findElement(By.xpath(`//*[@name="lookupIframe$ctl00_Main_EP_PROCESSODESDOBRAMENTOS_WID_PageControl_GERAL_GERAL_ctl27_ctl01_select_modal"]`)));
        //await fx.sleep(2000);
        await driver.executeScript(fx.clickByScript(`//*[@id="Resultado_FilterControl_SearchField"]`, json.Processo.Rito));
        await driver.executeScript(fx.clickByScript('//*[@id="Resultado_FilterControl_SearchButton"]/i'));
        await fx.sleep(2000);
        let rito = await driver.findElements(By.xpath(`//*[@id='Resultado_SimpleGrid']/tbody/tr/td[1]/a`));
        if (rito.length == 0) {
          let res = await persist.SalvarCampoNaoParametrizado("Rito", json.Processo.Rito, idDossie);
          throw new Error('Campo não parametrizado');
        }
        await driver.executeScript(fx.clickByScript(`//*[@id="Resultado_SimpleGrid"]/tbody/tr/td[1]/a`));
  
        await fx.sleep(1000);
        abas = await driver.getAllWindowHandles();
        await driver.switchTo().window(abas[1]);
        //await fx.sleep(1000);
      }
      
      //fase
      if (json.Processo.Fase != null) {
        await driver.executeScript(fx.clickByScript(`//div[contains(text(), "Fase")]/../../following-sibling::span//a`));
        //await fx.sleep(2000);
        await driver.switchTo().frame(driver.findElement(By.xpath(`//*[@name="lookupIframe$ctl00_Main_EP_PROCESSODESDOBRAMENTOS_WID_PageControl_GERAL_GERAL_ctl31_ctl01_select_modal"]`)));
        //await fx.sleep(2000);
        await driver.executeScript(fx.clickByScript(`//*[@id="Resultado_FilterControl_SearchField"]`, json.Processo.Fase));
        await driver.executeScript(fx.clickByScript('//*[@id="Resultado_FilterControl_SearchButton"]/i'));
        await fx.sleep(2000);
        let fase = await driver.findElements(By.xpath(`//*[@id='Resultado_SimpleGrid']/tbody/tr/td[1]/a`));
        if (fase.length == 0) {
          let res = await persist.SalvarCampoNaoParametrizado("Fase", json.Processo.Fase, idDossie);
          throw new Error('Campo não parametrizado');
        }
        await driver.executeScript(fx.clickByScript(`//*[@id="Resultado_SimpleGrid"]/tbody/tr/td[1]/a`));
  
        await fx.sleep(1000);
        abas = await driver.getAllWindowHandles();
        await driver.switchTo().window(abas[1]);
        //await fx.sleep(1000);
      }
            
      //orgão
      if (json.Processo.Orgao != null) {
        await driver.executeScript(fx.clickByScript(`//div[contains(text(), "Órgão")]/../../following-sibling::span//a`));
        //await fx.sleep(2000);
        await driver.switchTo().frame(driver.findElement(By.xpath(`//*[@name="lookupIframe$ctl00_Main_EP_PROCESSODESDOBRAMENTOS_WID_PageControl_GERAL_GERAL_ctl35_ctl01_select_modal"]`)));
        //await fx.sleep(2000);
        await driver.executeScript(fx.clickByScript(`//*[@id="Resultado_FilterControl_SearchField"]`, json.Processo.Orgao));
        await driver.executeScript(fx.clickByScript('//*[@id="Resultado_FilterControl_SearchButton"]/i'));
        await fx.sleep(2000);
        let orgao = await driver.findElements(By.xpath(`//*[@id='Resultado_SimpleGrid']/tbody/tr/td[1]/a`));
        if (orgao.length == 0) {
          let res = await persist.SalvarCampoNaoParametrizado("Órgão", json.Processo.Orgao, idDossie);
          throw new Error('Campo não parametrizado');
        }
        await driver.executeScript(fx.clickByScript(`//*[@id="Resultado_SimpleGrid"]/tbody/tr/td[1]/a`));
  
        await fx.sleep(1000);
        abas = await driver.getAllWindowHandles();
        await driver.switchTo().window(abas[1]);
        //await fx.sleep(1000);
      }
      
      //uf
      if (json.Processo.UF != null) {
        await driver.executeScript(fx.clickByScript(`//div[contains(text(), "UF")]/../../following-sibling::span//a`));
        //await fx.sleep(2000);
        await driver.switchTo().frame(driver.findElement(By.xpath(`//*[@name="lookupIframe$ctl00_Main_EP_PROCESSODESDOBRAMENTOS_WID_PageControl_GERAL_GERAL_ctl39_ctl01_select_modal"]`)));
        //await fx.sleep(2000);
        await driver.executeScript(fx.clickByScript(`//*[@id="Resultado_FilterControl_SearchField"]`, json.Processo.UF));
        await driver.executeScript(fx.clickByScript('//*[@id="Resultado_FilterControl_SearchButton"]/i'));
        await fx.sleep(2000);
        let uf = await driver.findElements(By.xpath(`//*[@id='Resultado_SimpleGrid']/tbody/tr/td[1]/a`));
        if (uf.length == 0) {
          let res = await persist.SalvarCampoNaoParametrizado("UF", json.Processo.UF, idDossie);
          throw new Error('Campo não parametrizado');
        }
        await driver.executeScript(fx.clickByScript(`//*[@id="Resultado_SimpleGrid"]/tbody/tr/td[1]/a`));
  
        await fx.sleep(1000);
        abas = await driver.getAllWindowHandles();
        await driver.switchTo().window(abas[1]);
        //await fx.sleep(1000);
      }
      
      //comarca
      if (json.Processo.Comarca != null) {
        await driver.executeScript(fx.clickByScript(`//div[contains(text(), "Comarca")]/../../following-sibling::span//a`));
        //await fx.sleep(2000);
        await driver.switchTo().frame(driver.findElement(By.xpath(`//*[@name="lookupIframe$ctl00_Main_EP_PROCESSODESDOBRAMENTOS_WID_PageControl_GERAL_GERAL_ctl43_ctl01_select_modal"]`)));
        //await fx.sleep(2000);
        await driver.executeScript(fx.clickByScript(`//*[@id="Resultado_FilterControl_SearchField"]`, json.Processo.Comarca));
        await driver.executeScript(fx.clickByScript('//*[@id="Resultado_FilterControl_SearchButton"]/i'));
        await fx.sleep(2000);
        let comarca = await driver.findElements(By.xpath(`//*[@id='Resultado_SimpleGrid']/tbody/tr/td[1]/a`));
        if (comarca.length == 0) {
          let res = await persist.SalvarCampoNaoParametrizado("Comarca", json.Processo.Comarca, idDossie);
          throw new Error('Campo não parametrizado');
        }
        await driver.executeScript(fx.clickByScript(`//*[@id="Resultado_SimpleGrid"]/tbody/tr/td[1]/a`));
  
        await fx.sleep(1000);
        abas = await driver.getAllWindowHandles();
        await driver.switchTo().window(abas[1]);
        //await fx.sleep(2000);
      }
      
      //jaDistribuidoJudicialmente
      if (json.Processo.DistribuidoJudicialmente != null) {
        if(json.Processo.DistribuidoJudicialmente.toUpperCase() == "NÃO"){
          await driver.executeScript(fx.clickByScript(`//input[@id="ctl00_Main_EP_PROCESSODESDOBRAMENTOS_WID_PageControl_GERAL_GERAL_DISTRIBUIDO_ctl03"]/..`));
        }
        if(json.Processo.DistribuidoJudicialmente.toUpperCase() == "SIM"){
          await driver.executeScript(fx.clickByScript(`//input[@id="ctl00_Main_EP_PROCESSODESDOBRAMENTOS_WID_PageControl_GERAL_GERAL_DISTRIBUIDO_ctl05"]/..`));
          await fx.sleep(2000);

          //foro
          if (json.Processo.Foro != null) {
            await driver.executeScript(fx.clickByScript(`//div[contains(text(), "Foro")]/../../following-sibling::span//a`));
            //await fx.sleep(2000);
            await driver.switchTo().frame(driver.findElement(By.xpath(`//*[@name="lookupIframe$ctl00_Main_EP_PROCESSODESDOBRAMENTOS_WID_PageControl_GERAL_GERAL_DISTRIBUIDO_2_ctl04_ctl01_select_modal"]`)));
            //await fx.sleep(2000);
            await driver.executeScript(fx.clickByScript(`//*[@id="Resultado_FilterControl_SearchField"]`, json.Processo.Foro));
            await driver.executeScript(fx.clickByScript('//*[@id="Resultado_FilterControl_SearchButton"]/i'));
            await fx.sleep(2000);
            let foro = await driver.findElements(By.xpath(`//*[@id='Resultado_SimpleGrid']/tbody/tr/td[1]/a`));
            if (foro.length == 0) {
              let res = await persist.SalvarCampoNaoParametrizado("Foro", json.Processo.Foro, idDossie);
              throw new Error('Campo não parametrizado');
            }
            await driver.executeScript(fx.clickByScript(`//*[@id="Resultado_SimpleGrid"]/tbody/tr/td[1]/a`));
      
            await fx.sleep(1000);
            abas = await driver.getAllWindowHandles();
            await driver.switchTo().window(abas[1]);
            //await fx.sleep(1000);
          }

          //juizo
          if (json.Processo.Juizo != null) {
            await driver.executeScript(fx.clickByScript(`//div[contains(text(), "Juízo")]/../../following-sibling::span//a`));
            //await fx.sleep(2000);
            await driver.switchTo().frame(driver.findElement(By.xpath(`//*[@name="lookupIframe$ctl00_Main_EP_PROCESSODESDOBRAMENTOS_WID_PageControl_GERAL_GERAL_DISTRIBUIDO_2_ctl08_ctl01_select_modal"]`)));
            //await fx.sleep(2000);
            await driver.executeScript(fx.clickByScript(`//*[@id="Resultado_FilterControl_SearchField"]`, json.Processo.Juizo));
            await driver.executeScript(fx.clickByScript('//*[@id="Resultado_FilterControl_SearchButton"]/i'));
            await fx.sleep(2000);
            
            let juizo = await driver.findElements(By.xpath(`//*[@id='Resultado_SimpleGrid']/tbody/tr/td[3]/a/text()[contains(translate(., "ABCDEFGHIJKLMNOPQRSTUVWXYZÁÉÍÓÚÀÈÌÒÙÃÕÂÊÎÔÛÇ", "abcdefghijklmnopqrstuvwxyzáéíóúàèìòùãõâêîôûç"), "` + json.Processo.Juizo.toLowerCase() + `")]/..`));
            if (juizo.length == 0) {
              let res = await persist.SalvarCampoNaoParametrizado("Juízo", json.Processo.Juizo, idDossie);
              throw new Error('Campo não parametrizado');
            }
            await driver.executeScript(fx.clickByScript(`//*[@id="Resultado_SimpleGrid"]/tbody/tr/td[3]/a/text()[contains(translate(., "ABCDEFGHIJKLMNOPQRSTUVWXYZÁÉÍÓÚÀÈÌÒÙÃÕÂÊÎÔÛÇ", "abcdefghijklmnopqrstuvwxyzáéíóúàèìòùãõâêîôûç"), "` + json.Processo.Juizo.toLowerCase() + `")]/..`));
      
            await fx.sleep(1000);
            abas = await driver.getAllWindowHandles();
            await driver.switchTo().window(abas[1]);
            //await fx.sleep(1000);
          }
        }        
        await fx.sleep(2000);
      }
      
      //data
      await driver.executeScript(fx.clickByScript(`//input[@id="ctl00_Main_EP_PROCESSODESDOBRAMENTOS_WID_PageControl_GERAL_GERAL_DISTRIBUIDO_2_DATADISTRIBUICAO_DATE"]`, json.Processo.Data));
      
      //numeroUnico
      if (json.Processo.EhNumeroUnico != null) {
        if(json.Processo.EhNumeroUnico.toUpperCase() == "NÃO"){
          await driver.executeScript(fx.clickByScript(`//input[@id="ctl00_Main_EP_PROCESSODESDOBRAMENTOS_WID_PageControl_GERAL_GERAL_NUMEROUNICO_ctl03"]/..`));
        }               
        await fx.sleep(2000);
      }
      
      //numeroDoProcesso
      await driver.executeScript(fx.clickByScript(`//*[@id="ctl00_Main_EP_PROCESSODESDOBRAMENTOS_WID_PageControl_GERAL_GERAL_NUMERODISTRIBUICAO"]`, json.Processo.NumeroUnico));

      //salvar      
      await driver.executeScript(fx.clickByScript(`//*[@id="ctl00_Main_EP_PROCESSODESDOBRAMENTOS_WID_FormContainer"]/div//*[@class="btn blue btn-save command-action predef-action editable"]`));
      await fx.sleep(2000);

      //verifico se houve erro ao Salvar
      let msgErro = await driver.findElements(By.xpath(`//*[@id="ctl00_Main_EP_PROCESSODESDOBRAMENTOS_WID_MsgUser_message"]`));
      if (msgErro.length > 0) {
        let msg = await driver.findElement(By.xpath(`//*[@id="ctl00_Main_EP_PROCESSODESDOBRAMENTOS_WID_MsgUser_message"]`)).getAttribute("innerHTML");
        throw new Error('Erro ao cadastrar o registro na tela de Processo - Msg:' + msg);
      }
  }
}