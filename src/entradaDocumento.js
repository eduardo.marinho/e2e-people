const { Builder, By, Key, until } = require('selenium-webdriver');
const fx = require('../comum/funcoes');
const persist = require('../persistencia/api-e2e-persistencia');

module.exports = {
  async Consultar(driver, json, idDossie) {
    try {
      let dir = await fx.criarPastaGuid(fx.guid());

      await driver.switchTo().frame(driver.findElement(By.xpath(`//*[@id="ModalCommand_modal"]/div/div/div/iframe`)));

      // Data
      await driver.executeScript(fx.clickByScript(`//input[@id="ctl00_Main_PR_PROCESSODOCUMENTOS_FORM_PageControl_GERAL_GERAL_DATADOCUMENTO_DATE"]`, json.Documento.DataDocumento));
      await fx.sleep(1000);

      // Seleciona tipo documento
      if (json.Documento.TipoDocumento != null) {
        await driver.executeScript(fx.clickByScript(`//div[@id="ctl00_Main_PR_PROCESSODOCUMENTOS_FORM_PageControl_GERAL_GERAL"]/div/div/div/label/div/div[contains(text(), "Tipo documento")]/../../following-sibling::span//a[@class="btn btn-default"]`));
        await fx.sleep(2000);
        await driver.switchTo().frame(driver.findElement(By.xpath(`//*[@name="lookupIframe$ctl00_Main_PR_PROCESSODOCUMENTOS_FORM_PageControl_GERAL_GERAL_ctl15_ctl01_select_modal"]`)));
        await fx.sleep(2000);
        await driver.executeScript(fx.clickByScript(`//*[@id="Resultado_FilterControl_SearchField"]`, json.Documento.TipoDocumento));
        await driver.executeScript(fx.clickByScript('//*[@id="Resultado_FilterControl_SearchButton"]/i'));
        await fx.sleep(2000);
        let documento = await driver.findElements(By.xpath(`//*[@id="Resultado_SimpleGrid"]/tbody/tr/td/a`));
        if (documento.length == 0) {
          let res = await persist.SalvarCampoNaoParametrizado("Documento", json.Documento.TipoDocumento, idDossie);
          throw new Error('Campo não parametrizado');
        }
        await driver.executeScript(fx.clickByScript(`//*[@id="Resultado_SimpleGrid"]/tbody/tr/td/a`));
  
        await fx.sleep(2000);
        abas = await driver.getAllWindowHandles();
        await driver.switchTo().window(abas[1]);
        await fx.sleep(1000);

        await driver.switchTo().frame(driver.findElement(By.xpath(`//*[@id="ModalCommand_modal"]/div/div/div/iframe`)));
        await fx.sleep(1000);
      }

      // Nome de documento
      await driver.executeScript(fx.clickByScript(`//input[@id="ctl00_Main_PR_PROCESSODOCUMENTOS_FORM_PageControl_GERAL_GERAL_NOME"]`, json.Documento.NomeDocumento));
      await fx.sleep(1000);
      
      // Seleciona pasta virtual
      if (json.Documento.PastaVirtual != null) {
        await driver.executeScript(fx.clickByScript(`//div[@id="ctl00_Main_PR_PROCESSODOCUMENTOS_FORM_PageControl_GERAL_GERAL"]/div/div/div/label/div/div[contains(text(), "Pasta virtual")]/../../following-sibling::span//a[@class="btn btn-default"]`));
        await fx.sleep(2000);
        await driver.switchTo().frame(driver.findElement(By.xpath(`//*[@name="lookupIframe$ctl00_Main_PR_PROCESSODOCUMENTOS_FORM_PageControl_GERAL_GERAL_ctl19_ctl01_select_modal"]`)));
        await fx.sleep(2000);
        await driver.executeScript(fx.clickByScript(`//*[@id="Resultado_FilterControl_SearchField"]`, json.Documento.PastaVirtual));
        await driver.executeScript(fx.clickByScript('//*[@id="Resultado_FilterControl_SearchButton"]/i'));
        await fx.sleep(2000);
        let pasta = await driver.findElements(By.xpath(`//*[@id="Resultado_SimpleGrid"]/tbody/tr/td/a`));
        if (pasta.length == 0) {
          let res = await persist.SalvarCampoNaoParametrizado("Pasta virtual", json.Documento.PastaVirtual, idDossie);
          throw new Error('Campo não parametrizado');
        }
        await driver.executeScript(fx.clickByScript(`//*[@id="Resultado_SimpleGrid"]/tbody/tr/td/a`));

        await fx.sleep(2000);
        abas = await driver.getAllWindowHandles();
        await driver.switchTo().window(abas[1]);
        await fx.sleep(1000);

        await driver.switchTo().frame(driver.findElement(By.xpath(`//*[@id="ModalCommand_modal"]/div/div/div/iframe`)));
        await fx.sleep(1000);
      }

      //faz o download do documento
      let ret = await persist.DocumentoDownload(json.Documento.LinkArquivo);
      //let nomeArquivo = "apiPersistencia_"+fx.guid()+"."+jsonParam.extensao;
      //let dir = path.resolve("./downloads");
      let caminhoArquivo = await fx.base64ToFile(json.Documento.NomeDocumento, ret.data,  dir);
      
      // Início de Upload de documentos
      await driver.executeScript(fx.clickByScript('//*[@id="ctl00_Main_PR_PROCESSODOCUMENTOS_FORM_PageControl_GERAL_GERAL_ARQUIVO"]/../div/button/i'));
      await fx.sleep(2000);
      await driver.switchTo().frame(driver.findElement(By.xpath(`//*[@id="file-uploader_modal"]/div/div/iframe`)));
      await fx.sleep(1000);
      let uploadElement = await driver.findElement(By.xpath(`//input[@id="file"]`));
      //await uploadElement.sendKeys(json.Documentos[a].LinkArquivo);
      await uploadElement.sendKeys(caminhoArquivo);        
      await fx.sleep(1000);
      await driver.executeScript(fx.clickByScript('//*[@id="btnUploadTheFile"]'));
      await fx.sleep(2000);
      abas = await driver.getAllWindowHandles();
      await driver.switchTo().window(abas[1]);
      await fx.sleep(1000);

      await driver.switchTo().frame(driver.findElement(By.xpath(`//*[@id="ModalCommand_modal"]/div/div/div/iframe`)));
      await fx.sleep(1000);

      //salvar    
      await driver.executeScript(fx.clickByScript(`//*[@class="btn blue btn-save command-action predef-action editable"]`));
      await fx.sleep(4000);

      //verifico se houve erro ao Salvar
      let msgErro = await driver.findElements(By.xpath(`//*[@id="ctl00_Main_PR_PROCESSODOCUMENTOS_FORM_MsgUser_message"]`));
      if (msgErro.length > 0) {
        let msg = await driver.findElement(By.xpath(`//*[@id="ctl00_Main_PR_PROCESSODOCUMENTOS_FORM_MsgUser_message"]`)).getAttribute("innerHTML");
        await fx.removerPastaDownload(dir);
        throw new Error('Erro ao cadastrar o registro na tela de Evento - Msg:' + msg);
      }

      await fx.removerPastaDownload(dir);

    } catch (e) {
        throw e;
    }
  }
}