const { Builder, By, Key, until } = require('selenium-webdriver');
const fx = require('../comum/funcoes');
const persist = require('../persistencia/api-e2e-persistencia');

const entradaDossie9 = require('../src/entradaDossies_9_cadastroPessoa');

module.exports = {
  async Consultar(driver, participantes, idDossie, ambiente) {         
    
      for(let a = 0; a < participantes.length; a++){
      
        await entradaDossie9.Consultar(driver, participantes[a], idDossie, ambiente);

        //participante
        if (participantes[a].NomeRazaoSocial != null) {
          await driver.executeScript(fx.clickByScript(`//div[contains(text(), "Participante")]/../../following-sibling::span//a[@class="btn btn-default"]`));
          //await fx.sleep(2000);
          await driver.switchTo().frame(driver.findElement(By.xpath(`//*[@name="lookupIframe$ctl00_Main_EP_PROCESSOPARTICIPANTES_FORM_PageControl_GERAL_GERAL_ctl12_ctl01_select_modal"]`)));
          //await fx.sleep(2000);
          if(participantes[a].Classe == "advogado"){
            await driver.executeScript(fx.clickByScript(`//*[@id="Resultado_FilterControl_SearchField"]`, participantes[a].NomeRazaoSocial));
          } else {
            await driver.executeScript(fx.clickByScript(`//*[@id="Resultado_FilterControl_SearchField"]`, fx.formataCPF_CNPJ(participantes[a].CpfCnpj)));
          }
          
          await driver.executeScript(fx.clickByScript('//*[@id="Resultado_FilterControl_SearchButton"]/i'));
          await fx.sleep(5000);
          let participante = await driver.findElements(By.xpath(`//*[@id="Resultado_SimpleGrid"]/tbody/tr/td/a`));
          if (participante.length == 0) {
            let res = await persist.SalvarCampoNaoParametrizado("Participante", participantes[a].NomeRazaoSocial, idDossie);
            throw new Error('Campo não parametrizado');
          }
          await driver.executeScript(fx.clickByScript(`//*[@id="Resultado_SimpleGrid"]/tbody/tr/td/a`));
    
          await fx.sleep(2000);
          abas = await driver.getAllWindowHandles();
          await driver.switchTo().window(abas[1]);
          //await fx.sleep(1000);
        }

        //classe        
        if(participantes[a].Classe.toUpperCase() == "CLIENTE"){
          await driver.executeScript(fx.clickByScript(`//input[@id="ctl00_Main_EP_PROCESSOPARTICIPANTES_FORM_PageControl_GERAL_GERAL_CLASSE_ctl03"]/..`));
        }
        else if(participantes[a].Classe.toUpperCase() == "ADVERSO"){
          await driver.executeScript(fx.clickByScript(`//input[@id="ctl00_Main_EP_PROCESSOPARTICIPANTES_FORM_PageControl_GERAL_GERAL_CLASSE_ctl05"]/..`));
        }
        else if(participantes[a].Classe.toUpperCase() == "TERCEIRO"){
          await driver.executeScript(fx.clickByScript(`//input[@id="ctl00_Main_EP_PROCESSOPARTICIPANTES_FORM_PageControl_GERAL_GERAL_CLASSE_ctl07"]/..`));
        }
        else if(participantes[a].Classe.toUpperCase() == "ADVOGADO"){
          await driver.executeScript(fx.clickByScript(`//input[@id="ctl00_Main_EP_PROCESSOPARTICIPANTES_FORM_PageControl_GERAL_GERAL_CLASSE_ctl09"]/..`));
          await fx.sleep(2000);
          await driver.executeScript(fx.clickByScript(`//div[contains(text(), "Tipo Advogado")]/..//label//text()[contains(translate(., "ABCDEFGHIJKLMNOPQRSTUVWXYZÁÉÍÓÚÀÈÌÒÙÃÕÂÊÎÔÛÇ", "abcdefghijklmnopqrstuvwxyzáéíóúàèìòùãõâêîôûç"), "` + participantes[a].Subclasse.toLowerCase() + `")]/..`));
        }                
        await fx.sleep(1000);

        if(participantes[a].Classe.toUpperCase() != "ADVOGADO"){
          //condicao
          if (participantes[a].Condicao != null) {
            await driver.executeScript(fx.clickByScript(`//div[contains(text(), "Condição")]/../../following-sibling::span//a`));
            //await fx.sleep(2000);
            await driver.switchTo().frame(driver.findElement(By.xpath(`//*[@name="lookupIframe$ctl00_Main_EP_PROCESSOPARTICIPANTES_FORM_PageControl_GERAL_GERAL_CLASSE_1_ctl04_ctl01_select_modal"]`)));
            //await fx.sleep(2000);
            await driver.executeScript(fx.clickByScript(`//*[@id="Resultado_FilterControl_SearchField"]`, participantes[a].Condicao));
            await driver.executeScript(fx.clickByScript('//*[@id="Resultado_FilterControl_SearchButton"]/i'));
            await fx.sleep(2000);
            let condicao = await driver.findElements(By.xpath(`//*[@id="Resultado_SimpleGrid"]/tbody/tr/td/a`));
            if (condicao.length == 0) {
              let res = await persist.SalvarCampoNaoParametrizado("Condição", participantes[a].Condicao, idDossie);
              throw new Error('Campo não parametrizado');
            }
            await driver.executeScript(fx.clickByScript(`//*[@id="Resultado_SimpleGrid"]/tbody/tr/td/a`));
      
            await fx.sleep(1000);
            abas = await driver.getAllWindowHandles();
            await driver.switchTo().window(abas[1]);
            //await fx.sleep(1000);          
          }
        }

        //principal        
        if(participantes[a].Principal && participantes[a].Principal.toUpperCase() == "SIM"){
          await driver.executeScript(fx.clickByScript(`//label[contains(text(), "Principal")]/../../label[@class="boolean-control mt-checkbox mt-checkbox-outline"]`));
          await fx.sleep(1000);
        }
        
        //se não for o último evento
        if(a < participantes.length -1){
          //salvar/Novo    
          await driver.executeScript(fx.clickByScript(`//*[@id="ctl00_Main_EP_PROCESSOPARTICIPANTES_FORM_FormContainer"]/div//*[@class="btn green btn-savenew command-action predef-action editable"]`));
          await fx.sleep(4000);

          //verifico se houve erro ao Salvar
          let msgErro = await driver.findElements(By.xpath(`//*[@id="ctl00_Main_EP_PROCESSOPARTICIPANTES_FORM_MsgUser_message"]`));
          if (msgErro.length > 0) {
            let msg = await driver.findElement(By.xpath(`//*[@id="ctl00_Main_EP_PROCESSOPARTICIPANTES_FORM_MsgUser_message"]`)).getAttribute("innerHTML");
            throw new Error('Erro ao cadastrar o registro na tela de Participantes - Msg:' + msg);
          }
        }  
      }
        
      //salvar      
      await driver.executeScript(fx.clickByScript(`//*[@id="ctl00_Main_EP_PROCESSOPARTICIPANTES_FORM_FormContainer"]/div//*[@class="btn blue btn-save command-action predef-action editable"]`));
      await fx.sleep(4000);

      //verifico se houve erro ao Salvar
      let msgErro = await driver.findElements(By.xpath(`//*[@id="ctl00_Main_EP_PROCESSOPARTICIPANTES_FORM_MsgUser_message"]`));
      if (msgErro.length > 0) {
        let msg = await driver.findElement(By.xpath(`//*[@id="ctl00_Main_EP_PROCESSOPARTICIPANTES_FORM_MsgUser_message"]`)).getAttribute("innerHTML");
        throw new Error('Erro ao cadastrar o registro na tela de Participantes - Msg:' + msg);
      }   
  }
}