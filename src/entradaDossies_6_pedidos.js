const { Builder, By, Key, until } = require('selenium-webdriver');
//const path = require('path');
const fx = require('../comum/funcoes');
const persist = require('../persistencia/api-e2e-persistencia');

module.exports = {
  async Consultar(driver, json, idDossie) {
    
      for(let a = 0; a < json.Pedidos.length; a++){

        // Seleciona pedido
        if (json.Pedidos[a] != null) {
          await driver.executeScript(fx.clickByScript(`//div[contains(text(), "Pedido")]/../../following-sibling::span//a`));
          //await fx.sleep(2000);
          await driver.switchTo().frame(driver.findElement(By.xpath(`//*[@name="lookupIframe$ctl00_Main_FORM_EP_PROCESSOS_PEDIDOS_PageControl_GERAL_GERAL_ctl12_ctl01_select_modal"]`)));
          //await fx.sleep(2000);
          await driver.executeScript(fx.clickByScript(`//*[@id="Resultado_FilterControl_SearchField"]`, json.Pedidos[a].Pedido));
          await driver.executeScript(fx.clickByScript('//*[@id="Resultado_FilterControl_SearchButton"]/i'));
          await fx.sleep(2000);
          let pedido = await driver.findElements(By.xpath(`//*[@id="Resultado_SimpleGrid"]/tbody/tr/td/a`));
          if (pedido.length == 0) {
            let res = await persist.SalvarCampoNaoParametrizado("Pedido", json.Pedidos[a].Pedido, idDossie);
            throw new Error('Campo não parametrizado');
          }
          await driver.executeScript(fx.clickByScript(`//*[@id="Resultado_SimpleGrid"]/tbody/tr/td/a`));
    
          await fx.sleep(1000);
          abas = await driver.getAllWindowHandles();
          await driver.switchTo().window(abas[1]);
          //await fx.sleep(1000);
        }
        
        // Data do pedido
        await driver.executeScript(fx.clickByScript(`//input[@id="ctl00_Main_FORM_EP_PROCESSOS_PEDIDOS_PageControl_GERAL_GERAL_DATAPEDIDO_DATE"]`, json.Pedidos[a].DataPedido));
        
        // Valor do pedido
        await driver.executeScript(fx.clickByScript(`//input[@id="ctl00_Main_FORM_EP_PROCESSOS_PEDIDOS_PageControl_GERAL_GERAL_VALORPEDIDO"]`, json.Pedidos[a].ValorPedido));
                
        // Seleciona risco
        if (json.Pedidos[a] != null) {
          await driver.executeScript(fx.clickByScript(`//div[contains(text(), "Risco")]/../../following-sibling::span//a`));
          //await fx.sleep(2000);
          await driver.switchTo().frame(driver.findElement(By.xpath(`//*[@name="lookupIframe$ctl00_Main_FORM_EP_PROCESSOS_PEDIDOS_PageControl_GERAL_GERAL_ctl27_ctl01_select_modal"]`)));
          //await fx.sleep(2000);
          await driver.executeScript(fx.clickByScript(`//*[@id="Resultado_FilterControl_SearchField"]`, json.Pedidos[a].Risco));
          await driver.executeScript(fx.clickByScript('//*[@id="Resultado_FilterControl_SearchButton"]/i'));
          await fx.sleep(2000);
          let risco = await driver.findElements(By.xpath(`//*[@id="Resultado_SimpleGrid"]/tbody/tr/td/a`));
          if (risco.length == 0) {
            let res = await persist.SalvarCampoNaoParametrizado("Risco", json.Pedidos[a].Risco, idDossie);
            throw new Error('Campo não parametrizado');
          }
          await driver.executeScript(fx.clickByScript(`//*[@id="Resultado_SimpleGrid"]/tbody/tr/td/a`));
    
          await fx.sleep(1000);
          abas = await driver.getAllWindowHandles();
          await driver.switchTo().window(abas[1]);
          //await fx.sleep(1000);
        }

        // descrição do pedido
        if (json.Pedidos[a].Descricao != null) {
          await driver.executeScript(fx.clickByScript(`//textarea[@id="ctl00_Main_FORM_EP_PROCESSOS_PEDIDOS_PageControl_GERAL_GERAL_DESCRICAO"]`, json.Pedidos[a].Descricao));
        }
            
        //se não for o último evento
        if(a < json.Pedidos.length -1){
          //salvar/Novo    
          await driver.executeScript(fx.clickByScript(`//*[@id="ctl00_Main_FORM_EP_PROCESSOS_PEDIDOS_FormContainer"]/div//*[@class="btn green btn-savenew command-action predef-action editable"]`));
          await fx.sleep(2000);

          //verifico se houve erro ao Salvar
          let msgErro = await driver.findElements(By.xpath(`//*[@id="ctl00_Main_FORM_EP_PROCESSOS_PEDIDOS_MsgUser_message"]`));
          if (msgErro.length > 0) {
            let msg = await driver.findElement(By.xpath(`//*[@id="ctl00_Main_FORM_EP_PROCESSOS_PEDIDOS_MsgUser_message"]`)).getAttribute("innerHTML");
            throw new Error('Erro ao cadastrar o registro na tela de Pedidos - Msg:' + msg);
          }
        }
      };

      //salvar    
      await driver.executeScript(fx.clickByScript(`//*[@id="ctl00_Main_FORM_EP_PROCESSOS_PEDIDOS_FormContainer"]/div//*[@class="btn blue btn-save command-action predef-action editable"]`));
      await fx.sleep(2000);

      //verifico se houve erro ao Salvar
      let msgErro = await driver.findElements(By.xpath(`//*[@id="ctl00_Main_FORM_EP_PROCESSOS_PEDIDOS_MsgUser_message"]`));
      if (msgErro.length > 0) {
        let msg = await driver.findElement(By.xpath(`//*[@id="ctl00_Main_FORM_EP_PROCESSOS_PEDIDOS_MsgUser_message"]`)).getAttribute("innerHTML");
        throw new Error('Erro ao cadastrar o registro na tela de Pedidos - Msg:' + msg);
      }   
  }
}