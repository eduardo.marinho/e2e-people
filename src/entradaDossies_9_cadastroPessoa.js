const { Builder, By, Key, until } = require('selenium-webdriver');
const fx = require('../comum/funcoes');
const persist = require('../persistencia/api-e2e-persistencia');

module.exports = {
  async Consultar(driver, participante, idDossie, ambiente) { 
    //await driver.executeScript(fx.clickByScript(`//span[text()="Atalhos"]/../../ul[@class="sub-menu"]/li/a/span[text()="Pessoas"]`));
    //await fx.sleep(2000);

    if(ambiente == "prod"){
      await driver.executeScript("window.open('https://tjmjuridico.santanderbr.corp/JURIDICO/jur/a/GN_PESSOAS/Grid.aspx?i=GN_PESSOAS&m=BARRA_DE_ATALHOS','_blank')");
    } else {
      await driver.executeScript("window.open('https://tjbjuridico.santanderbr.pre.corp/JURIDICO_HOM/jur/a/GN_PESSOAS/Grid.aspx?i=GN_PESSOAS&m=BARRA_DE_ATALHOS','_blank')");
    }
    
    await fx.sleep(5000);
    
    abas = await driver.getAllWindowHandles();
    await driver.switchTo().window(abas[2]);
    //await fx.sleep(1000);

  //limpo as pesquisas
  await driver.executeScript(fx.limparCampo(`//input[@id="ctl00_Main_GN_PESSOAS_GRID_FilterControl_GERAL_1__NOME"]`));
  await driver.executeScript(fx.limparCampo(`//input[@id="ctl00_Main_GN_PESSOAS_GRID_FilterControl_GERAL_3__CGC"]`));
  await driver.executeScript(fx.limparCampo(`//input[@id="ctl00_Main_GN_PESSOAS_GRID_FilterControl_GERAL_2__CPFNUMERO"]`));

  if(participante.Classe == "advogado"){
    //nome
    await driver.executeScript(fx.clickByScript(`//input[@id="ctl00_Main_GN_PESSOAS_GRID_FilterControl_GERAL_1__NOME"]`, participante.NomeRazaoSocial));
    //await fx.sleep(1000); 
  } else if(participante.CpfCnpj.length > 11){
    //cnpj
    await driver.executeScript(fx.clickByScript(`//input[@id="ctl00_Main_GN_PESSOAS_GRID_FilterControl_GERAL_3__CGC"]`, participante.CpfCnpj));
    //await fx.sleep(1000);
  } else {
    //cpf
    await driver.executeScript(fx.clickByScript(`//input[@id="ctl00_Main_GN_PESSOAS_GRID_FilterControl_GERAL_2__CPFNUMERO"]`, participante.CpfCnpj));
    //await fx.sleep(1000);
  }     

    await driver.executeScript(fx.clickByScript('//*[@id="ctl00_Main_GN_PESSOAS_GRID_FilterControl_FilterButton"]/i'));
    await fx.sleep(5000);

    let participantesCadastrados = await driver.findElements(By.xpath(`//*[@id="ctl00_Main_GN_PESSOAS_GRID_SimpleGrid"]/tbody/tr/td/a`));
    if (participantesCadastrados.length == 0) {
      //novo
      await driver.executeScript(fx.clickByScript(`//div[@id="ctl00_Main_GN_PESSOAS_GRID_toolbar"]/a[@class="btn green btn-new command-action predef-action editable"]`));
      await fx.sleep(3000);
           
      //nome
      await driver.executeScript(fx.clickByScript(`//div[contains(text(),"Nome")]/../../../span/input`, participante.NomeRazaoSocial));
      //await fx.sleep(1000);  
       
      //aba classificação
      await driver.executeScript(fx.clickByScript(`//ul[@class="nav nav-tabs"]/li/a[contains(text(),"Classificação")]`));
      //await fx.sleep(1000);

      //classificacao
      if(participante.Condicao == "ESCRITÓRIO ADVERSO"){
        await driver.executeScript(fx.clickByScript(`//h4[contains(text(),"Classificação")]/../div//text()[contains(translate(., "ABCDEFGHIJKLMNOPQRSTUVWXYZÁÉÍÓÚÀÈÌÒÙÃÕÂÊÎÔÛÇ", "abcdefghijklmnopqrstuvwxyzáéíóúàèìòùãõâêîôûç"), "advogado adverso")]/..`));
      } else {
        await driver.executeScript(fx.clickByScript(`//h4[contains(text(),"Classificação")]/../div//text()[contains(translate(., "ABCDEFGHIJKLMNOPQRSTUVWXYZÁÉÍÓÚÀÈÌÒÙÃÕÂÊÎÔÛÇ", "abcdefghijklmnopqrstuvwxyzáéíóúàèìòùãõâêîôûç"), "` + participante.Classificacao.toLowerCase() + `")]/..`));
      }
      
      //aba tipo
      await driver.executeScript(fx.clickByScript(`//ul[@class="nav nav-tabs"]/li/a[contains(text(),"Tipo")]`));
      await fx.sleep(1000);

      //tipo
      if(participante.Classe == "advogado"){
        if(participante.Condicao == "ESCRITÓRIO ADVERSO"){
          await driver.executeScript(fx.clickByScript(`//div[contains(text(),"Tipo")]/../div//label[text()="Pessoa Jurídica"]`));
          await fx.sleep(1000);

          //registro profissional
          if (participante.OabCnpj != null) {
            await driver.executeScript(fx.clickByScript(`//div[contains(text(),"CNPJ")]/../../../span/input`, participante.OabCnpj));
          }
        } else {
          await driver.executeScript(fx.clickByScript(`//div[contains(text(),"Tipo")]/../div//label[text()="Pessoa Física"]`));
          await fx.sleep(1000);

          //registro profissional
          if (participante.OabCnpj != null) {
            await driver.executeScript(fx.clickByScript(`//div[contains(text(),"Número registro profissional")]/../../../span/input`, participante.OabCnpj));
          }
          
          //órgão regulamentador
          if (participante.OabOrgao != null) {
            await driver.executeScript(fx.clickByScript(`//div[contains(text(), "Órgão Regulamentador")]/../../following-sibling::span//a`));
            //await fx.sleep(2000);
            await driver.switchTo().frame(driver.findElement(By.xpath(`//*[@name="lookupIframe$ctl00_Main_GN_PESSOAS_FORM_PageControl_TIPO_TIPO_TIPO_1_ctl126_ctl01_select_modal"]`)));
            //await fx.sleep(2000);
            await driver.executeScript(fx.clickByScript(`//*[@id="Resultado_FilterControl_SearchField"]`, participante.OabOrgao));
            await driver.executeScript(fx.clickByScript('//*[@id="Resultado_FilterControl_SearchButton"]/i'));
            await fx.sleep(2000);
            let orgao = await driver.findElements(By.xpath(`//*[@id="Resultado_SimpleGrid"]/tbody/tr/td/a`));
            if (orgao.length == 0) {
              let res = await persist.SalvarCampoNaoParametrizado("Condição", participante.OabOrgao, idDossie);
              throw new Error('Campo não parametrizado');
            }
            await driver.executeScript(fx.clickByScript(`//*[@id="Resultado_SimpleGrid"]/tbody/tr/td/a`));
      
            await fx.sleep(1000);
            abas = await driver.getAllWindowHandles();
            await driver.switchTo().window(abas[2]);
            //await fx.sleep(1000);          
          }


          //estado órgão regulamentador
          if (participante.OabUf != null) {
            await driver.executeScript(fx.clickByScript(`//div[contains(text(), "Estado órgão regulamentador")]/../../following-sibling::span//a`));
            //await fx.sleep(2000);
            await driver.switchTo().frame(driver.findElement(By.xpath(`//*[@name="lookupIframe$ctl00_Main_GN_PESSOAS_FORM_PageControl_TIPO_TIPO_TIPO_1_ctl130_ctl01_select_modal"]`)));
            //await fx.sleep(2000);
            await driver.executeScript(fx.clickByScript(`//*[@id="Resultado_FilterControl_SearchField"]`, participante.OabUf));
            await driver.executeScript(fx.clickByScript('//*[@id="Resultado_FilterControl_SearchButton"]/i'));
            await fx.sleep(2000);
            let estadoOrgao = await driver.findElements(By.xpath(`//*[@id="Resultado_SimpleGrid"]/tbody/tr/td/a`));
            if (estadoOrgao.length == 0) {
              let res = await persist.SalvarCampoNaoParametrizado("Condição", participante.OabUf, idDossie);
              throw new Error('Campo não parametrizado');
            }
            await driver.executeScript(fx.clickByScript(`//*[@id="Resultado_SimpleGrid"]/tbody/tr/td/a`));
      
            await fx.sleep(1000);
            abas = await driver.getAllWindowHandles();
            await driver.switchTo().window(abas[2]);
            //await fx.sleep(1000);          
          }
        }
      } else if(participante.CpfCnpj.length > 11){
        await driver.executeScript(fx.clickByScript(`//div[contains(text(),"Tipo")]/../div//label[text()="Pessoa Jurídica"]`));
        await fx.sleep(1000);
        //cnpj
        await driver.executeScript(fx.clickByScript(`//div[contains(text(),"CNPJ")]/../../../span/input`, participante.CpfCnpj));
      } else {
        await driver.executeScript(fx.clickByScript(`//div[contains(text(),"Tipo")]/../div//label[text()="Pessoa Física"]`));
        await fx.sleep(1000);
        //cpf
        await driver.executeScript(fx.clickByScript(`//div[contains(text(),"CPF")]/../../../span/input`, participante.CpfCnpj));
      }
      await fx.sleep(1000);

      //salvar      
      await driver.executeScript(fx.clickByScript(`//*[@class="btn blue btn-save command-action predef-action editable"]`));
      await fx.sleep(3000);   
      
      //verifico se houve erro ao Salvar
      let msgConfirmacao = await driver.findElements(By.xpath(`//div[@class="alert alert-block alert-info alert-request-confirmation fade in"]/p[contains(text(),"Existe outra pessoa de mesmo nome, deseja continuar")]`));
      if (msgConfirmacao.length > 0) {
        await driver.executeScript(fx.clickByScript(`//*[@id="ctl00_Main_GN_PESSOAS_FORM_RQTrue"]`));
        await fx.sleep(3000);        
      }
      
    }

    await driver.close();
    //await fx.sleep(1000);
    abas = await driver.getAllWindowHandles();
    await driver.switchTo().window(abas[1]);
    //await fx.sleep(1000);
  }
}