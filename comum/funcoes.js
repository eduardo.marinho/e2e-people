const fs = require('fs-extra');
const path = require('path');
const downloadsFolder = require('downloads-folder');

exports.sleep = sleep;
exports.Hora = Hora;
exports.clickByScript = clickByScript;
exports.criarPastaGuid = criarPastaGuid;
exports.removerPastaDownload = removerPastaDownload;
exports.guid = guid;
exports.base64ToFile = base64ToFile;
exports.limparCampo = limparCampo;
exports.formataCPF = formataCPF;
exports.formataCNPJ = formataCNPJ;
exports.formataCPF_CNPJ = formataCPF_CNPJ;

async function sleep(millis) {
    return new Promise(resolve => setTimeout(resolve, millis));
}

function Hora() {
    let d = new Date();
    let hh = (`00${d.getHours()}`).slice(-2);
    let mm = (`00${d.getMinutes()}`).slice(-2);
    let ss = (`00${d.getSeconds()}`).slice(-2);
    return `${hh}:${mm}:${ss}`;
}

function clickByScript(path, value){
    let returnElement = `function _x(fintro){
        let el = document.evaluate(fintro, document, null, XPathResult.ANY_TYPE, null);
        let ret = el.iterateNext();
        return ret;
      }`;

    let str;
    
    if(value){
        str = returnElement + "_x('" + path + "').value='" + value + "';"        
    } else {
        str = returnElement + "_x('" + path + "').click();"
    }
    
    return str;
}

function limparCampo(path){
    let returnElement = `function _x(fintro){
        let el = document.evaluate(fintro, document, null, XPathResult.ANY_TYPE, null);
        let ret = el.iterateNext();
        return ret;
      }`;

    let str;
    str = returnElement + "_x('" + path + "').value='';"
    return str;
}

function guid(){
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}

function s4(){
    return Math.floor((1 + Math.random()) * 0X10000)
    .toString(16)
    .substring(1);
}

async function criarPastaGuid(guid){
    var downloadFolder = downloadsFolder();
    var dir = downloadFolder + "/" + guid;
    if (!fs.existsSync(dir)){
        await fs.mkdirSync(dir,{recursive: true});
    }
    let pastaGuid = path.resolve(dir);
    return pastaGuid;
}
    
async function removerPastaDownload(dir){
    if(dir){
        fs.rmdirSync(dir, { recursive: true });
    }
}

async function base64ToFile(fileName, base64,  diretorio){
    let base64Image = base64.split(';base64,').pop();

    if (!fs.existsSync(diretorio)){
        fs.mkdirSync(diretorio,{recursive: true});
    }

    let dsLink = `${diretorio}/${fileName}`;
    fs.writeFileSync(dsLink, base64Image, { encoding: 'base64' });

    console.log(dsLink);

    return dsLink;
}

function formataCPF(cpf){
    //retira os caracteres indesejados...
    cpf = cpf.replace(/[^\d]/g, "");

    //realizar a formatação...
    return cpf.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/, "$1.$2.$3-$4");
}

function formataCNPJ(cnpj){
    //retira os caracteres indesejados...
    cnpj = cnpj.replace(/[^\d]/g, "");

    //realizar a formatação...
    return cnpj.replace(/^(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/, "$1.$2.$3/$4-$5");
}

function formataCPF_CNPJ(cpfCnpj){
    //retira os caracteres indesejados...
    cpfCnpj = cpfCnpj.replace(/[^\d]/g, "");

    //realizar a formatação...
    if(cpfCnpj.length > 11){
        return cpfCnpj.replace(/^(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/, "$1.$2.$3/$4-$5");
    } else {
        return cpfCnpj.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/, "$1.$2.$3-$4");
    }
}