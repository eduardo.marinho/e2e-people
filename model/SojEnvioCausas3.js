module.exports = {
    async JsonModelo() {
        let SojEnvioCausasJson =
        {
            "GestorJuridico": {
                "IdServico": 123456789,
                "CdCausa": "CIV0000001"
            },
            "EntradaDossie": {
                "Coligada": "BANCO SANTANDER (BRASIL) S/A",
                "DataFato": "20/05/2021",
                "Departamento": "JURÍDICO",
                "Categoria": "JURÍDICO TRABALHISTA",
                "Subcategoria": "NÃO DEFINIDO",
                "TipoAcao": "RECLAMAÇÃO TRABALHISTA",
                "ObjetoPadrao": null,
                "Assunto": "NÃO DEFINIDO",
                "Natureza": "CONDENATÓRIA",
                "BancoEmpresaOrigem": "BANCO SANTANDER (BRASIL) S/A",
                "TipoCaptura": "Captura Antecipada",
                "MotivoRelevancia": null,
                "ValorCausa": 706560.76,
                "PastaAntiga": "TRA0000042",
                "Risco": "POSSÍVEL",
                "DossieUrgente": false
            },
            "Processo": {
                "Numero": "DIFINIR",
                "Principal": true,
                "Natureza": "CONDENATÓRIA",
                "Instancia": "1ª Instância",
                "Rito": "Ordinário",
                "Orgao": "DEFINIR",
                "Fase": "FASE 1 - CONHECIMENTO VT",
                "UF": "PE",
                "Comarca": "RECIFE",
                "DistribuidoJudicialmente": true,
                "NumeroUnico": "0000515-10.2021.5.06.0001",
                "Juizo": "",
                "Data": "02/07/2021"
            },
            "Eventos": [
                {
                    "Data": "16/10/2021",
                    "Hora": "00:00",
                    "Descricao": "Captação Antecipada"
                },
                {
                    "Data": "16/10/2021",
                    "Hora": "15:00",
                    "Descricao": "Audiência"
                }
            ],
            "Participantes": {
                "Clientes": [
                    {
                        "CpfCnpj": "01234567890",
                        "NomeRazaoSocial": "BANCO SANTANDER (BRASIL) S/A",
                        "Principal": true,
                        "Condicao": "RECLAMADO"
                    }
                ],
                "Adversos": [
                    {
                        "CpfCnpj": "01234567890",
                        "NomeRazaoSocial": "E A M F",
                        "Principal": true,
                        "Condicao": "RECLAMANTE"
                    }
                ],
                "Advogados": [
                    {
                        "CpfCnpj": "01234567890",
                        "NomeRazaoSocial": "ESCRITÓRIO SA",
                        "Condicao": "ESCRITORIO CONTRARIO"
                    }
                ],
                "Terceiros": [
                    {
                        "CpfCnpj": "01234567890",
                        "NomeRazaoSocial": "E A M F",
                        "Condicao": "EMPRESA TERCEIRA (PRESTADOR)"
                    },
                    {
                        "CpfCnpj": "01234567890",
                        "NomeRazaoSocial": "E A M F",
                        "Condicao": "TERCEIRO - TOMADOR DE SERVIÇOS"
                    },
                    {
                        "CpfCnpj": "01234567890",
                        "NomeRazaoSocial": "E A M F",
                        "Condicao": "TERCEIRO - PARADIGMA"
                    }
                ]
            },
            "Documentos": [
                {
                    "Data": "19/07/2021",
                    "Nome": "std_0000515-10.2021.5.06.0001.pdf",
                    "Tipo": "CONTRA FÉ",
                    "Link": "http=//e-impservico.gestorjuridico.com.br/download.aspx?idDoc=2620&causa=&sigla=E2E&V=44428"
                }
            ],
            "Pedidos": [
                {
                    "Descricao": "TESTE",
                    "Data": "02/07/2021",
                    "Valor": 0
                }
            ]
        }
        return SojEnvioCausasJson;
    }
}

