module.exports = {
  async JsonModelo() {
let SojEnvioCausasJson =
{
    "GestorJuridico": {
      "CdCausa": "CI20000001",
      "IdServico": 19062
    },
    "EntradaDossie": {
      "Coligada": "ABN AMRO Administradora de Cartões de Crédito Ltda",
      "DataFato": null,
      "Departamento": "JURIDICO",
      "Categoria": "Jurídico Cível",
      "Subcategoria": "Crítico",
      "TipoAcao": "Indenizatória",
      "ObjetoPadrao": "Ação Comum",
      "Assunto": "Ação Comum",
      "Natureza": "ACP",
      "Risco": "POSSÍVEL",
      "BancoEmpresaOrigem": "ABN AMRO Administradora de Cartões de Crédito Ltda",
      "TipoCaptura": "Captura Antecipada",
      "ValorCausa": "30000,00",
      "TipoDesligamento": "Terceiro",
      "PastaAntiga": "CIV1802954",
      "Relevante": "SIM",
      "MotivoRelevancia": "Alerta",
      "DossieUrgente": "Sim"
    },
    "Processo": {
      "Processo": "Ação Indenizatória",
      "Principal": "SIM",
      "Natureza": "JUDICIAL",
      "Instancia": "1ª Instância",
      "Rito": "Juizado Especial",
      "Fase": "1ª Fase - Antes da Sentença",
      "Orgao": "JEC",
      "UF": "BA",
      "Comarca": "Salvador (BA)",
      "DistribuidoJudicialmente": "SIM",
      "Juizo": "18ª",
      "Data": "13/10/2021",
      "EhNumeroUnico": "NÃO",
      "NumeroUnico": "0000000-00.0000.0.00.0001"
    },
    "Eventos": [
      {
        "DataEvento": "14/01/2022",
        "Hora": "00:00",
        "Evento": "Captura antecipada da inicial"
      },
      {
        "DataEvento": "04/02/2022",
        "Hora": "16:30",
        "Evento": "Audiência"
      }
    ],
    "Participantes": {
      "Clientes": [
        {
          "NomeRazaoSocial": "Cliente Teste",
          "CpfCnpj": "30619496000147",
          "Condicao": "Réu",
          "Principal": "SIM"
        }
      ],
      "Adversos": [
        {
          "NomeRazaoSocial": "Adverso Teste",
          "CpfCnpj": "20072083263",
          "Condicao": "Autor",
          "Principal": "SIM"
        }
      ],
      "Advogados": [
        {
          "NomeRazaoSocial": "Advogado adverso",
          "CpfCnpj": "12345",
          "Condicao": "Advogado adverso",
          "Principal": "SIM"
        },
        {
          "NomeRazaoSocial": "Advogado credenciado",
          "CpfCnpj": "12345",
          "Condicao": "Advogado credenciado",
          "Principal": "SIM"
        },
        {
          "NomeRazaoSocial": "Advogado colaborador",
          "OabCnpj": "12345",
          "Condicao": "Advogado colaborador",
          "Principal": "SIM"
        }        
      ],
      "Terceiros": [
        
      ]
    },
    "Documentos": [
      {
        "DataDocumento": "21/10/2021",
        "NomeDocumento": "teste1.pdf",
        "TipoDocumento": "INICIAL",
        "LinkArquivo": "C:\\Desenvolvimento\\teste.txt",
        "PastaVirtual": "Corpo Principal"
      },
      {
        "DataDocumento": "22/10/2021",
        "NomeDocumento": "teste2.pdf",
        "TipoDocumento": "INICIAL",
        "LinkArquivo": "C:\\Desenvolvimento\\teste2.txt",
        "PastaVirtual": "Corpo Principal"
      }
    ],
    "Pedidos": [
      {
        "Pedido": "Ajuda de Custo",
        "DataPedido": "14/10/2021",
        "ValorPedido": "1,89",
        "Risco": "Possível"
      }
    ]
  }
  return SojEnvioCausasJson;
    }
}