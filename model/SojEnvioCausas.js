module.exports = {
    async JsonModelo() {
        let SojEnvioCausasJson =
        {
            "GestorJuridico": {
               "CdCausa": "CI20028329",
               "IdServico": 101710
            },
            "EntradaDossie": {
               "Coligada": "BANCO SANTANDER (BRASIL) S/A",
               "DataFato": "09/12/2019",
               "Departamento": "JURIDICO",
               "Categoria": "JURÍDICO CIVEL",
               "Subcategoria": "MASSIFICADOS",
               "TipoAcao": "INDENIZATÓRIA",
               "ObjetoPadrao": "CRÉDITO CONSIGNADO",
               "Assunto": "CRÉDITO CONSIGNADO",
               "Natureza": "JUDICIAL",
               "Risco": "POSSÍVEL",
               "BancoEmpresaOrigem": "BANCO SANTANDER (BRASIL) S/A",
               "TipoCaptura": "Captura Antecipada",
               "ValorCausa": "122397,98",
               "TipoDesligamento": "INDENIZATÓRIA",
               "PastaAntiga": "OLE0063672",
               "Relevante": "SIM",
               "MotivoRelevancia": "Civ - Olé",
               "DossieUrgente": "NÃO"
            },
            "Processo": {
               "Processo": "Ação Indenizatória",
               "Principal": "SIM",
               "Natureza": "JUDICIAL",
               "Instancia": "1ª Instância",
               "Rito": "Ordinário",
               "Fase": "1ª Fase - Antes da Sentença",
               "Orgao": "Vara Civel",
               "UF": "RJ",
               "Comarca": "Rio de Janeiro (RJ)",
               "DistribuidoJudicialmente": "SIM",
               "Juizo": "23ª",
               "Data": "30/10/2020",
               "EhNumeroUnico": "SIM",
               "NumeroUnico": "0221989-75.2020.8.19.0001"
            },
            "Eventos": [
               {
                  "DataEvento": "27/01/2022",
                  "Hora": "00:00",
                  "Evento": "CAPTURA ANTECIPADA DA INICIAL"
               },
               {
                  "DataEvento": "27/01/2022",
                  "Hora": "00:00",
                  "Evento": "Eventos Críticos"
               }
            ],
            "Participantes": {
               "Clientes": [
                  {
                     "NomeRazaoSocial": "FLP PROMOTORA DE VENDAS LTDA",
                     "CpfCnpj": "28414136000167",
                     "Condicao": "RÉU",
                     "Principal": "NÃO"
                  },
                  {
                     "NomeRazaoSocial": "BANCO OLE BONSUCESSO CONSIGNADO S A",
                     "CpfCnpj": "71371686000175",
                     "Condicao": "RÉU",
                     "Principal": "NÃO"
                  }
               ],
               "Adversos": [
                  {
                     "NomeRazaoSocial": "ROSANGÊLA MARIA FURTADO SANTÂNGELO",
                     "CpfCnpj": "93194013734",
                     "Condicao": "AUTOR",
                     "Principal": "SIM"
                  }
               ],
               "Advogados": [
                  {
                     "NomeRazaoSocial": "BRUNO MEDEIROS DURÃO",
                     "OabCnpj": "152.121",
                     "Condicao": "ADVOGADO ADVERSO"
                  },
                  {
                    "NomeRazaoSocial": "Advogado colaborador",
                    "OabCnpj": "123123",
                    "Condicao": "ADVOGADO COLABORADOR"
                 },
                 {
                    "NomeRazaoSocial": "Advogado credenciado",
                    "OabCnpj": "567567",
                    "Condicao": "ADVOGADO CREDENCIADO"
                 }
               ],
               "Terceiros": []
            },
            "Documentos": [
               {
                  "DataDocumento": "03/11/2020",
                  "NomeDocumento": "std_0221989-75.2020.8.19.0001.pdf",
                  "TipoDocumento": "INICIAL E DOCUMENTOS DO AUTOR",
                  "LinkArquivo": "https://docs.gestorjuridico.com.br/Documento/Download?idDoc=48628&causa=&sigla=E2E&V=44601",
                  "PastaVirtual": "Corpo Principal"
               },
               {
                  "DataDocumento": "27/01/2022",
                  "NomeDocumento": "DOSSIÊ_02.02.033.0003228873-20.pdf",
                  "TipoDocumento": "COMPROVANTE DE CADASTRO BENNER",
                  "LinkArquivo": "https://docs.gestorjuridico.com.br/Documento/Download?idDoc=49781&causa=&sigla=E2E&V=44601",
                  "PastaVirtual": "Corpo Principal"
               }
            ],
            "Pedidos": [
               {
                  "Pedido": "DANO MATERIAL",
                  "DataPedido": "30/10/2020",
                  "ValorPedido": "0,00",
                  "Risco": "Possivel"
               },
               {
                  "Pedido": "DANO MORAL",
                  "DataPedido": "30/10/2020",
                  "ValorPedido": "0,00",
                  "Risco": "Possivel"
               },
               {
                  "Pedido": "OBRIGAÇÃO DE FAZER",
                  "DataPedido": "30/10/2020",
                  "ValorPedido": "0,00",
                  "Risco": "Possivel"
               },
               {
                  "Pedido": "OBRIGAÇÃO DE NÃO FAZER",
                  "DataPedido": "30/10/2020",
                  "ValorPedido": "0,00",
                  "Risco": "Possivel"
               },
               {
                  "Pedido": "SUCUMBENCIA",
                  "DataPedido": "30/10/2020",
                  "ValorPedido": "12239,79",
                  "Risco": "Possivel"
               }
            ]
         }
        return SojEnvioCausasJson;
    }
}

